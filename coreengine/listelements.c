#include "listelements.h"
#include "../config.h"
#include "../services/epgtimer.h"
#include <sstream>
#include <algorithm>

// eLeMenuDefaultIT, eLeMenuDefaultST
#define TOKEN_LMD_IT  eLeMenuDefaultIT
#define TOKEN_LMD_ST  eLeMenuDefaultST

#define ADD_TOKEN_LMD_IT(entry, value)   tokenContainer->AddIntToken((int)TOKEN_LMD_IT::entry, value)
#define ADD_TOKEN_LMD_ST(entry, value)   tokenContainer->AddStringToken((int)TOKEN_LMD_ST::entry, value)

// eLeGroupListIT, eLeGroupListST
#define TOKEN_LGL_IT  eLeGroupListIT
#define TOKEN_LGL_ST  eLeGroupListST

#define ADD_TOKEN_LGL_IT(entry, value)   tokenContainer->AddIntToken((int)TOKEN_LGL_IT::entry, value)
#define ADD_TOKEN_LGL_ST(entry, value)   tokenContainer->AddStringToken((int)TOKEN_LGL_ST::entry, value)

// eLeChannelListIT, eLeChannelListST
#define TOKEN_LCL_IT  eLeChannelListIT
#define TOKEN_LCL_ST  eLeChannelListST

#define ADD_TOKEN_LCL_IT(entry, value)   tokenContainer->AddIntToken((int)TOKEN_LCL_IT::entry, value)
#define ADD_TOKEN_LCL_ST(entry, value)   tokenContainer->AddStringToken((int)TOKEN_LCL_ST::entry, value)

// eLeDisplayTracksIT, eLeDisplayTracksST
#define TOKEN_LDT_IT  eLeDisplayTracksIT
#define TOKEN_LDT_ST  eLeDisplayTracksST

#define ADD_TOKEN_LDT_IT(entry, value)   tokenContainer->AddIntToken((int)TOKEN_LDT_IT::entry, value)
#define ADD_TOKEN_LDT_ST(entry, value)   tokenContainer->AddStringToken((int)TOKEN_LDT_ST::entry, value)

/*-------------------------------------------------------------------------------------------------------*/

// eCeMenuTimersIT, eCeMenuTimersST
#define TOKEN_CMT_IT  eCeMenuTimersIT
#define TOKEN_CMT_ST  eCeMenuTimersST

#define ADD_TOKEN_CMT_IT(entry, value)   tokenContainer->AddIntToken((int)TOKEN_CMT_IT::entry, value)
#define ADD_TOKEN_CMT_ST(entry, value)   tokenContainer->AddStringToken((int)TOKEN_CMT_ST::entry, value)

// eLeMenuTimersIT, eLeMenuTimersST
#define TOKEN_LMT_IT  eLeMenuTimersIT
#define TOKEN_LMT_ST  eLeMenuTimersST

#define ADD_TOKEN_LMT_IT(entry, value)   tokenContainer->AddIntToken((int)TOKEN_LMT_IT::entry, value)
#define ADD_TOKEN_LMT_ST(entry, value)   tokenContainer->AddStringToken((int)TOKEN_LMT_ST::entry, value)

/*----------------------------------------------------------------------------------------------*/
/* fill tokens common for eCeMenuTimersIT, eCeMenuTimersST, eLeMenuTimersIT and eLeMenuTimersST */
/*----------------------------------------------------------------------------------------------*/
#define DEF_TOK_CONT_MT(token_it, token_st)                                        				\
    {                                                                   						\
	/* fill tokens for eCeMenuTimersIT, eCeMenuTimersST, eLeMenuTimersIT and eLeMenuTimersST */	\
    tokenContainer->DefineStringToken("{title}", (int)token_st::title);                         \
    tokenContainer->DefineStringToken("{timerstart}", (int)token_st::timerstart);               \
    tokenContainer->DefineStringToken("{timerstop}", (int)token_st::timerstop);                 \
    tokenContainer->DefineStringToken("{day}", (int)token_st::day);                             \
    tokenContainer->DefineStringToken("{dayname}", (int)token_st::dayname);                     \
    tokenContainer->DefineStringToken("{monthname}", (int)token_st::monthname);                 \
    tokenContainer->DefineStringToken("{channelname}", (int)token_st::channelname);             \
    tokenContainer->DefineStringToken("{channelid}", (int)token_st::channelid);                 \
    tokenContainer->DefineStringToken("{eventtitle}", (int)token_st::eventtitle);               \
    tokenContainer->DefineStringToken("{eventstart}", (int)token_st::eventstart);               \
    tokenContainer->DefineStringToken("{eventstop}", (int)token_st::eventstop);	                \
    tokenContainer->DefineStringToken("{state}", (int)token_st::state);                         \
    tokenContainer->DefineStringToken("{stateinfo}", (int)token_st::stateinfo);                 \
    tokenContainer->DefineStringToken("{action}", (int)token_st::action);                       \
    tokenContainer->DefineStringToken("{vdrname}", (int)token_st::vdrname);                     \
    tokenContainer->DefineStringToken("{type}", (int)token_st::type);                           \
    tokenContainer->DefineIntToken("{month}", (int)token_it::month);                            \
    tokenContainer->DefineIntToken("{year}", (int)token_it::year);                              \
    tokenContainer->DefineIntToken("{channellogoexists}", (int)token_it::channellogoexists);    \
    tokenContainer->DefineIntToken("{channelnumber}", (int)token_it::channelnumber);            \
    tokenContainer->DefineIntToken("{flagactive}", (int)token_it::flagactive);                  \
    tokenContainer->DefineIntToken("{flaginstant}", (int)token_it::flaginstant);                \
    tokenContainer->DefineIntToken("{flagvps}", (int)token_it::flagvps);                        \
    tokenContainer->DefineIntToken("{flagrecording}", (int)token_it::flagrecording);            \
    tokenContainer->DefineIntToken("{flagpending}", (int)token_it::flagpending);                \
    tokenContainer->DefineIntToken("{isvdrrunning}", (int)token_it::isvdrrunning);              \
    tokenContainer->DefineIntToken("{isremote}", (int)token_it::isremote);                      \
}

/*-----------------------------------------------------------------*/
/* Add token for cCeMenuTimers::Parse() and cLeMenuTimers::Parse() */
/*-----------------------------------------------------------------*/
#define ADD_TOKEN_MT(token_it)                                        				  			\
    {                                                                                           \
    tokenContainer->AddIntToken((int)token_it::flagactive, timer->HasFlags(tfActive));          \
    tokenContainer->AddIntToken((int)token_it::flaginstant, timer->HasFlags(tfInstant));        \
    tokenContainer->AddIntToken((int)token_it::flagvps, timer->HasFlags(tfVps));                \
    tokenContainer->AddIntToken((int)token_it::flagrecording, timer->Recording());              \
    tokenContainer->AddIntToken((int)token_it::flagpending, timer->Pending());                  \
	}

/*-------------------------------------------------------------------------------------------------------*/

// eCeMenuRecordingsIT, eCeMenuRecordingsST
#define TOKEN_CMR_IT  eCeMenuRecordingsIT
#define TOKEN_CMR_ST  eCeMenuRecordingsST

#define ADD_TOKEN_CMR_IT(entry, value)   tokenContainer->AddIntToken((int)TOKEN_CMR_IT::entry, value)
#define ADD_TOKEN_CMR_ST(entry, value)   tokenContainer->AddStringToken((int)TOKEN_CMR_ST::entry, value)

// eLeMenuRecordingsIT, eLeMenuRecordingsST
#define TOKEN_LMR_IT  eLeMenuRecordingsIT
#define TOKEN_LMR_ST  eLeMenuRecordingsST

#define ADD_TOKEN_LMR_IT(entry, value)   tokenContainer->AddIntToken((int)TOKEN_LMR_IT::entry, value)
#define ADD_TOKEN_LMR_ST(entry, value)   tokenContainer->AddStringToken((int)TOKEN_LMR_ST::entry, value)

/*-------------------------------------------------------------------------------------------------------*/
/* fill tokens for eCeMenuRecordingsIT, eCeMenuRecordingsST, eLeMenuRecordingsIT and eLeMenuRecordingsST */
/*-------------------------------------------------------------------------------------------------------*/
#define DEF_TOK_CONT_MR(token_it, token_st)                                        				   		\
    {                                                                   						   		\
    tokenContainer->DefineStringToken("{name}", (int)token_st::name);                                   \
    tokenContainer->DefineStringToken("{epgname}", (int)token_st::epgname);                             \
    tokenContainer->DefineStringToken("{shorttext}", (int)token_st::shorttext);                         \
    tokenContainer->DefineStringToken("{description}", (int)token_st::description);                     \
    tokenContainer->DefineStringToken("{date}", (int)token_st::date);                                   \
    tokenContainer->DefineStringToken("{time}", (int)token_st::time);                                   \
    tokenContainer->DefineStringToken("{durationminutes}", (int)token_st::durationminutes);             \
    tokenContainer->DefineStringToken("{durationeventminutes}", (int)token_st::durationeventminutes);   \
    tokenContainer->DefineStringToken("{thumbnailpath}", (int)token_st::thumbnailpath);                 \
    tokenContainer->DefineStringToken("{posterpath}", (int)token_st::posterpath);                       \
    tokenContainer->DefineIntToken("{new}", (int)token_it::isnew);                                      \
    tokenContainer->DefineIntToken("{percentseen}", (int)token_it::percentseen);                        \
    tokenContainer->DefineIntToken("{watched}", (int)token_it::watched);                                \
    tokenContainer->DefineIntToken("{cutted}", (int)token_it::cutted);                                  \
    tokenContainer->DefineIntToken("{folder}", (int)token_it::folder);                                  \
    tokenContainer->DefineIntToken("{daynumeric}", (int)token_it::daynumeric);                          \
    tokenContainer->DefineIntToken("{month}", (int)token_it::month);                                    \
    tokenContainer->DefineIntToken("{year}", (int)token_it::year);                                      \
    tokenContainer->DefineIntToken("{duration}", (int)token_it::duration);                              \
    tokenContainer->DefineIntToken("{durationhours}", (int)token_it::durationhours);                    \
    tokenContainer->DefineIntToken("{durationevent}", (int)token_it::durationevent);                    \
    tokenContainer->DefineIntToken("{durationeventhours}", (int)token_it::durationeventhours);          \
    tokenContainer->DefineIntToken("{numrecordingsfolder}", (int)token_it::numrecordingsfolder);        \
    tokenContainer->DefineIntToken("{newrecordingsfolder}", (int)token_it::newrecordingsfolder);        \
    tokenContainer->DefineIntToken("{hasposterthumbnail}", (int)token_it::hasposterthumbnail);          \
    tokenContainer->DefineIntToken("{thumbnailwidth}", (int)token_it::thumbnailwidth);                  \
    tokenContainer->DefineIntToken("{thumbnailheight}", (int)token_it::thumbnailheight);                \
    tokenContainer->DefineIntToken("{hasposter}", (int)token_it::hasposter);                            \
    tokenContainer->DefineIntToken("{posterwidth}", (int)token_it::posterwidth);                        \
    tokenContainer->DefineIntToken("{posterheight}", (int)token_it::posterheight);                      \
    tokenContainer->DefineIntToken("{fps}", (int)token_it::framesPerSecond);                            \
    tokenContainer->DefineIntToken("{isHD}", (int)token_it::isHD);                                      \
    tokenContainer->DefineIntToken("{isUHD}", (int)token_it::isUHD);                                    \
    tokenContainer->DefineIntToken("{isRadio}", (int)token_it::isRadio);                                \
    tokenContainer->DefineIntToken("{isRecording}", (int)token_it::isRecording);                        \
    tokenContainer->DefineIntToken("{isInUse}", (int)token_it::isInUse);                                \
    tokenContainer->DefineIntToken("{errors}", (int)token_it::errors);                                  \
    tokenContainer->DefineStringToken("{recchannelname}", (int)token_st::recchannelname);               \
    tokenContainer->DefineStringToken("{recchannelid}", (int)token_st::recchannelid);                   \
}

/*---------------------------------------------------------------------------------*/
/* Add first SW part for cCeMenuRecordings::Parse() and cLeMenuRecordings::Parse() */
/*---------------------------------------------------------------------------------*/
#define ADD_TOKEN_MR1(token_it)                                        				  			\
    {                                                                                           \
    tokenContainer->AddIntToken((int)token_it::percentseen, percSeen);               			\
    tokenContainer->AddIntToken((int)token_it::newrecordingsfolder, New);            			\
    tokenContainer->AddIntToken((int)token_it::numrecordingsfolder, total);          			\
    tokenContainer->AddIntToken((int)token_it::cutted, usedRecording->IsEdited());   			\
																								\
    bool watched = false;                                                                       \
    if (usedRecording->IsEdited()) {                                                            \
        if (percSeen >= 85)                                                                     \
            watched = true;                                                                     \
    } else {                                                                                    \
        int watchedLimit = recDuration * 85 / 100 - (Setup.MarginStop + 5)*60;                  \
        int watchedTime = percSeen * recDuration / 100;                                         \
        if (watchedLimit > 0 && watchedTime > 0 && (watchedTime > watchedLimit))                \
            watched = true;                                                                     \
    }                                                                                           \
    tokenContainer->AddIntToken((int)token_it::watched, watched);                    			\
	}

/*----------------------------------------------------------------------------------*/
/* Add second SW part for cCeMenuRecordings::Parse() and cLeMenuRecordings::Parse() */
/*----------------------------------------------------------------------------------*/
#define ADD_TOKEN_MR2(token_it, token_st)                                        				  					\
    {                                                                                           					\
    time_t startTime = event->StartTime();                                                                          \
    if (!startTime)                                                                                                 \
        startTime = usedRecording->Start();                                                                         \
    struct tm * sStartTime = localtime(&startTime);                                                                 \
    tokenContainer->AddIntToken((int)token_it::year, sStartTime->tm_year + 1900);                                   \
    tokenContainer->AddIntToken((int)token_it::daynumeric, sStartTime->tm_mday);                                    \
    tokenContainer->AddIntToken((int)token_it::month, sStartTime->tm_mon+1);                                        \
																													\
    int duration = event->Duration() / 60;                                                                          \
    recDuration = (recDuration>0)?(recDuration / 60):0;                                                             \
    tokenContainer->AddStringToken((int)token_st::date, *recDate);                                                  \
    tokenContainer->AddStringToken((int)token_st::time, *recTime);                                                  \
    tokenContainer->AddIntToken((int)token_it::duration, recDuration);                                              \
    tokenContainer->AddIntToken((int)token_it::durationhours, recDuration / 60);                                    \
    tokenContainer->AddStringToken((int)token_st::durationminutes, *cString::sprintf("%.2d", recDuration%60));      \
    tokenContainer->AddIntToken((int)token_it::durationevent, duration);                                            \
    tokenContainer->AddIntToken((int)token_it::durationeventhours, duration / 60);                                  \
    tokenContainer->AddStringToken((int)token_st::durationeventminutes, *cString::sprintf("%.2d", duration%60));    \
																													\
    tokenContainer->AddStringToken((int)token_st::shorttext, info->ShortText());                                    \
    tokenContainer->AddStringToken((int)token_st::description, info->Description());                                \
    tokenContainer->AddIntToken((int)token_it::framesPerSecond, info->FramesPerSecond());                           \
    tokenContainer->AddIntToken((int)token_it::isHD, RecordingIsHD(event)); /* detect HD from 'info' */             \
    tokenContainer->AddIntToken((int)token_it::isUHD, RecordingIsUHD(event)); /* detect UHD from 'info' */          \
    tokenContainer->AddIntToken((int)token_it::isRadio, RecordingIsRadio(event, info->FramesPerSecond())); /* detect Radio from 'info' and FPS */ \
    tokenContainer->AddIntToken((int)token_it::isRecording, usedRecording->IsInUse() & ruTimer);                    \
    tokenContainer->AddIntToken((int)token_it::isInUse, usedRecording->IsInUse());                                  \
    tokenContainer->AddIntToken((int)token_it::errors, errors);                                                     \
	}

/*-------------------------------------------------------------------------------------------------------*/

// eCeMenuMainIT, eCeMenuMainST
#define TOKEN_CMM_IT  eCeMenuMainIT
#define TOKEN_CMM_ST  eCeMenuMainST

#define ADD_TOKEN_CMM_IT(entry, value)   tokenContainer->AddIntToken((int)TOKEN_CMM_IT::entry, value)
#define ADD_TOKEN_CMM_ST(entry, value)   tokenContainer->AddStringToken((int)TOKEN_CMM_ST::entry, value)

// eLeMenuMainIT, eLeMenuMainST
#define TOKEN_LMM_IT  eLeMenuMainIT
#define TOKEN_LMM_ST  eLeMenuMainST

#define ADD_TOKEN_LMM_IT(entry, value)   tokenContainer->AddIntToken((int)TOKEN_LMM_IT::entry, value)
#define ADD_TOKEN_LMM_ST(entry, value)   tokenContainer->AddStringToken((int)TOKEN_LMM_ST::entry, value)

/*-----------------------------------------------------------------------------------*/
/* fill tokens common for eCeMenuMainIT, eCeMenuMainST, eLeMenuMainIT, eLeMenuMainST */
/*-----------------------------------------------------------------------------------*/
#define DEF_TOK_CONT_MM(token_st)                                                				   \
    {                                                                                              \
    tokenContainer->DefineStringToken("{label}", (int)token_st::label);                            \
    tokenContainer->DefineStringToken("{number}", (int)token_st::number);                          \
    tokenContainer->DefineStringToken("{icon}", (int)token_st::icon);                              \
}

/*-------------------------------------------------------------*/
/* Add token for cCeMenuMain::Parse() and cLeMenuMain::Parse() */
/*-------------------------------------------------------------*/
#define ADD_TOKEN_MM(token_st)                                        				  			   \
    {                                                                                              \
	tokenContainer->AddStringToken((int)token_st::number, number);                                 \
	tokenContainer->AddStringToken((int)token_st::label, label);                                   \
	tokenContainer->AddStringToken((int)token_st::icon, imgCache->GetIconName(label).c_str());     \
	}

/*-------------------------------------------------------------------------------------------------------*/

// eCeMenuSchedulesIT, eCeMenuSchedulesST
#define TOKEN_CMS_IT  eCeMenuSchedulesIT
#define TOKEN_CMS_ST  eCeMenuSchedulesST

#define ADD_TOKEN_CMS_IT(entry, value)   tokenContainer->AddIntToken((int)TOKEN_CMS_IT::entry, value)
#define ADD_TOKEN_CMS_ST(entry, value)   tokenContainer->AddStringToken((int)TOKEN_CMS_ST::entry, value)

// eLeMenuSchedulesIT, eLeMenuSchedulesST
#define TOKEN_LMS_IT  eLeMenuSchedulesIT
#define TOKEN_LMS_ST  eLeMenuSchedulesST

#define ADD_TOKEN_LMS_IT(entry, value)   tokenContainer->AddIntToken((int)TOKEN_LMS_IT::entry, value)
#define ADD_TOKEN_LMS_ST(entry, value)   tokenContainer->AddStringToken((int)TOKEN_LMS_ST::entry, value)

/*-------------------------------------------------------------------------------------------------------*/
/* fill tokens common for eCeMenuSchedulesIT, eCeMenuSchedulesST, eLeMenuSchedulesIT, eLeMenuSchedulesST */
/*-------------------------------------------------------------------------------------------------------*/
#define DEF_TOK_CONT_MS(token_it, token_st)                                        			   \
    {                                                                                          \
    tokenContainer->DefineStringToken("{title}", (int)token_st::title);                        \
    tokenContainer->DefineStringToken("{shorttext}", (int)token_st::shorttext);                \
    tokenContainer->DefineStringToken("{start}", (int)token_st::start);                        \
    tokenContainer->DefineStringToken("{stop}", (int)token_st::stop);                          \
    tokenContainer->DefineStringToken("{day}", (int)token_st::day);                            \
    tokenContainer->DefineStringToken("{date}", (int)token_st::date);                          \
    tokenContainer->DefineIntToken("{month}", (int)token_it::month);                           \
    tokenContainer->DefineIntToken("{year}", (int)token_it::year);                             \
    tokenContainer->DefineIntToken("{running}", (int)token_it::running);                       \
    tokenContainer->DefineIntToken("{elapsed}", (int)token_it::elapsed);                       \
    tokenContainer->DefineIntToken("{duration}", (int)token_it::duration);                     \
    tokenContainer->DefineStringToken("{channelname}", (int)token_st::channelname);            \
    tokenContainer->DefineStringToken("{channelid}", (int)token_st::channelid);                \
    tokenContainer->DefineStringToken("{timertype}", (int)token_st::timertype);                \
    tokenContainer->DefineIntToken("{whatson}", (int)token_it::whatson);                       \
    tokenContainer->DefineIntToken("{whatsonnow}", (int)token_it::whatsonnow);                 \
    tokenContainer->DefineIntToken("{whatsonnext}", (int)token_it::whatsonnext);               \
    tokenContainer->DefineIntToken("{whatsonfavorites}", (int)token_it::whatsonfavorites);     \
    tokenContainer->DefineIntToken("{timerpartitial}", (int)token_it::timerpartitial);         \
    tokenContainer->DefineIntToken("{timerfull}", (int)token_it::timerfull);                   \
    tokenContainer->DefineIntToken("{isremotetimer}", (int)token_it::isremotetimer);           \
    tokenContainer->DefineIntToken("{daynumeric}", (int)token_it::daynumeric);                 \
    tokenContainer->DefineIntToken("{durationhours}", (int)token_it::durationhours);           \
    tokenContainer->DefineIntToken("{channelnumber}", (int)token_it::channelnumber);           \
    tokenContainer->DefineIntToken("{channellogoexists}", (int)token_it::channellogoexists);   \
}

/*-------------------------------------------------------------------------------------------------------*/

// eLeMenuChannelsIT, eLeMenuChannelsST
#define TOKEN_LMC_IT  eLeMenuChannelsIT
#define TOKEN_LMC_ST  eLeMenuChannelsST

#define ADD_TOKEN_LMC_IT(entry, value)   tokenContainer->AddIntToken((int)TOKEN_LMC_IT::entry, value)
#define ADD_TOKEN_LMC_ST(entry, value)   tokenContainer->AddStringToken((int)TOKEN_LMC_ST::entry, value)

// eCeMenuChannelsIT, eCeMenuChannelsST
#define TOKEN_CMC_IT  eCeMenuChannelsIT
#define TOKEN_CMC_ST  eCeMenuChannelsST

#define ADD_TOKEN_CMC_IT(entry, value)   tokenContainer->AddIntToken((int)TOKEN_CMC_IT::entry, value)
#define ADD_TOKEN_CMC_ST(entry, value)   tokenContainer->AddStringToken((int)TOKEN_CMC_ST::entry, value)

/*---------------------------------------------------------------------------------------------------*/
/* fill tokens common for eCeMenuChannelsIT, eCeMenuChannelsST, eLeMenuChannelsIT, eLeMenuChannelsST */
/*---------------------------------------------------------------------------------------------------*/
#define DEF_TOK_CONT_MC(token_it, token_st)                                        				   \
    {                                                                                              \
    tokenContainer->DefineStringToken("{name}", (int)token_st::name);                              \
    tokenContainer->DefineStringToken("{channelid}", (int)token_st::channelid);                    \
    tokenContainer->DefineStringToken("{source}", (int)token_st::source);                          \
    tokenContainer->DefineStringToken("{sourcedescription}", (int)token_st::sourcedescription);    \
    tokenContainer->DefineStringToken("{position}", (int)token_st::position);                      \
    tokenContainer->DefineStringToken("{presenteventtitle}", (int)token_st::presenteventtitle);    \
    tokenContainer->DefineStringToken("{presenteventstart}", (int)token_st::presenteventstart);    \
    tokenContainer->DefineStringToken("{presenteventstop}", (int)token_st::presenteventstop);      \
    tokenContainer->DefineIntToken("{number}", (int)token_it::number);                             \
    tokenContainer->DefineIntToken("{transponder}", (int)token_it::transponder);                   \
    tokenContainer->DefineIntToken("{frequency}", (int)token_it::frequency);                       \
    tokenContainer->DefineIntToken("{channellogoexists}", (int)token_it::channellogoexists);       \
    tokenContainer->DefineIntToken("{isAtsc}", (int)token_it::isAtsc);                             \
    tokenContainer->DefineIntToken("{isCable}", (int)token_it::isCable);                           \
    tokenContainer->DefineIntToken("{isSat}", (int)token_it::isSat);                               \
    tokenContainer->DefineIntToken("{isTerr}", (int)token_it::isTerr);                             \
    tokenContainer->DefineIntToken("{isEncrypted}", (int)token_it::isEncrypted);                   \
    tokenContainer->DefineIntToken("{isRadio}", (int)token_it::isRadio);                           \
    tokenContainer->DefineIntToken("{isHD}", (int)token_it::isHD);                                 \
    tokenContainer->DefineIntToken("{isUHD}", (int)token_it::isUHD);                               \
	}

/*---------------------------------------------------------------------*/
/* Add token for cCeMenuChannels::Parse() and cLeMenuChannels::Parse() */
/*---------------------------------------------------------------------*/
#define ADD_TOKEN_MC(token_it, token_st)                                        						\
	{																									\
	tokenContainer->AddIntToken((int)token_it::number, channel->Number());     							\
	tokenContainer->AddIntToken((int)token_it::transponder, channel->Transponder());    				\
	tokenContainer->AddIntToken((int)token_it::frequency, channel->Frequency());    					\
	tokenContainer->AddIntToken((int)token_it::isEncrypted, channel->Ca());     						\
	tokenContainer->AddIntToken((int)token_it::isRadio, isRadio);     									\
	tokenContainer->AddIntToken((int)token_it::isHD, isHD);     										\
	tokenContainer->AddIntToken((int)token_it::isUHD, isUHD);     										\
																										\
    tokenContainer->AddStringToken((int)token_st::name, channel->Name());                               \
    cString channelID = channel->GetChannelID().ToString();                                             \
    tokenContainer->AddStringToken((int)token_st::channelid, *channelID);                               \
    tokenContainer->AddIntToken((int)token_it::channellogoexists, imgCache->LogoExists(*channelID));    \
																										\
    /* Channel Source Information */                                                                    \
    const cSource *source = Sources.Get(channel->Source());                                             \
    if (source) {                                                                                       \
        tokenContainer->AddStringToken((int)token_st::source, *cSource::ToString(source->Code()));      \
        tokenContainer->AddStringToken((int)token_st::sourcedescription, source->Description());        \
        tokenContainer->AddIntToken((int)token_it::isAtsc, source->IsAtsc(source->Code()));             \
        tokenContainer->AddIntToken((int)token_it::isCable, source->IsCable(source->Code()));           \
        tokenContainer->AddIntToken((int)token_it::isSat, source->IsSat(source->Code()));               \
        tokenContainer->AddIntToken((int)token_it::isTerr, source->IsTerr(source->Code()));             \
		}                                                                                               \
	}

/*-------------------------------------------------------------------------------------------------------*/

/******************************************************************
* cListElement
******************************************************************/
cListElement::cListElement(void) {
    dirty = false;
    num = -1;
    current = false;
    wasCurrent = false;
    selectable = false;
    selectedFromTop = true;
    suppressAnimation = false;
    listShifter = NULL;
    currentElement = NULL;
    menuCat = mcUndefined;
    orientation = eOrientation::vertical;
};

cListElement::cListElement(const cListElement &other) : cViewElement(other) {
    num = -1;
    current = false;
    wasCurrent = false;
    selectable = false;
    selectedFromTop = true;
    suppressAnimation = false;
    listShifter = NULL;
    currentElement = NULL;
    orientation = eOrientation::vertical;
}

void cListElement::SetCurrent(bool cur) {
    if (!cur && current)
        wasCurrent = true;
    else
        wasCurrent = false;
    current = cur;
}

void cListElement::Close(void) {
    if (current && currentElement) {
        currentElement->Close();
    }
    cViewElement::Close();
    dirty = false;
}

void cListElement::Clear(bool forceClearBackground) {
    if (current && currentElement) {
        currentElement->Close();
    }
    cViewElement::Clear(forceClearBackground);
    dirty = false;
}

void cListElement::WakeCurrent(void) {
    if (currentElement) {
        currentElement->WakeUp();
    }
}

void cListElement::Render(void) {
    if (!dirty || blocked)
        return;

    if (attribs->DoDebug())
        Debug();
    bool animated = Fading() || Shifting();
    for (cAreaNode *node = areaNodes.First(); node; node = areaNodes.Next(node)) {
        //Check redraw of already scrolling list element
        if (drawn && node->Scrolling() && node->ScrollingStarted()) {
            if (DoScroll()) {
                //current list element
                continue;
            }
        }
        //don't clear animated list element if it was current
        //and animation was not suppressed because of cleared list
        sdOsd->Lock();
        if (animated && wasCurrent && !suppressAnimation) {
            node->ClearWithoutIndicators();
        } else {
            node->Clear();
        }
        sdOsd->Unlock();
        if (!node->Execute())
            continue;
        sdOsd->Lock();
        node->Render();
        sdOsd->Unlock();

        if (DoScroll() && node->Scrolling() && !node->ScrollingStarted()) {
            cArea *scrollArea = node->ScrollingArea();
            if (scrollArea) {
                cScroller *scroller = new cScroller(scrollArea);
                scrollers.push_back(scroller);
                cView::AddAnimation(scroller);
            }
        }
    }
    dirty = false;
    drawn = true;
    StartListAnimation();
}

int cListElement::ShiftDistance(void) {
    if (orientation == eOrientation::horizontal)
        return container.Width();
    return container.Height();
}

eOrientation cListElement::ShiftOrientation(void) {
    return orientation;
}

void cListElement::SetIndicatorPosition(cPoint &position) {
    for (cAreaNode *node = areaNodes.First(); node; node = areaNodes.Next(node)) {
        sdOsd->Lock();
        node->SetIndicatorPosition(position);
        sdOsd->Unlock();
    }
}

void cListElement::SetTransparency(int transparency, bool force) {
    for (cAreaNode *node = areaNodes.First(); node; node = areaNodes.Next(node)) {
        sdOsd->Lock();
        node->SetIndicatorTransparency(transparency);
        sdOsd->Unlock();
    }
}

char *cListElement::ParseSeparator(const char *text) {
    const char *start = text;
    while (*start && (*start == '-' || *start == ' ' || *start == 9)) {
        start++;
    }
    const char *end = start;
    while (*end && *end != '-') {
        end++;
    }
    int len = end - start;
    char *ret = (char*)malloc(len);
    memset(ret, 0, len);
    strncpy(ret, start, len-1);
    return ret;
}

void cListElement::StopListAnimation(void) {
    if (listShifter) {
        cView::RemoveAnimation(listShifter);
        listShifter = NULL;
    }
    if (fader) {
        cView::RemoveAnimation(fader);
        fader = NULL;
    }
}

void cListElement::StartListAnimation(void) {
    if (suppressAnimation)
        return;
    if (!Fading() && !Shifting())
        return;
    listShifter = NULL;
    fader = NULL;
    if (current) {
        if (ShiftTime() > 0) {
            listShifter = new cListShifter((cListShiftable*)this);
            listShifter->SetDirection(selectedFromTop);
            cView::AddAnimation(listShifter, true);
        } else if (FadeTime() > 0) {
            fader = new cFader((cFadable*)this);
            cView::AddAnimation(fader, true);
        }
    }
    if (wasCurrent) {
        if (ShiftTime() > 0) {
            listShifter = new cListShifter((cListShiftable*)this);
            listShifter->SetDirection(selectedFromTop);
            listShifter->SetShiftOut();
            cView::AddAnimation(listShifter, false);
        } else if (FadeTime() > 0) {
            fader = new cFader((cFadable*)this);
            fader->SetFadeOut();
            fader->SetHideWhenFinished();
            cView::AddAnimation(fader, false);
        }
    }
}

/******************************************************************
* cCurrentElement
******************************************************************/
cCurrentElement::cCurrentElement(void) {
    listX = 0;
    listY = 0;
    listWidth = 0;
    listHeight = 0;
    listNum = 0;
}

void cCurrentElement::SetListPosition(int x, int y, int width, int height, int num) {
    listX = x;
    listY = y;
    listWidth = width;
    listHeight = height;
    listNum = num;
}

void cCurrentElement::SetListTokens(skindesignerapi::cTokenContainer *tokenContainer) {
    tokenContainer->AddIntToken(0, listX - container.X());
    tokenContainer->AddIntToken(1, listY - container.Y());
    tokenContainer->AddIntToken(2, listWidth);
    tokenContainer->AddIntToken(3, listHeight);
    tokenContainer->AddIntToken(4, listNum);
}

/******************************************************************
* cLeMenuDefault
******************************************************************/
cLeMenuDefault::cLeMenuDefault(void) {
    text = NULL;
    colX = NULL;
    colWidths = NULL;
    plugName = NULL;
}

cLeMenuDefault::cLeMenuDefault(const cLeMenuDefault &other) : cListElement(other) {
    text = NULL;
    colX = NULL;
    colWidths = NULL;
    plugName = NULL;
}

cLeMenuDefault::~cLeMenuDefault(void) {

}

void cLeMenuDefault::SetTokenContainer(void) {
    tokenContainer = new skindesignerapi::cTokenContainer();

    tokenContainer->DefineStringToken("{column1}", (int)TOKEN_LMD_ST::column1);
    tokenContainer->DefineStringToken("{column2}", (int)TOKEN_LMD_ST::column2);
    tokenContainer->DefineStringToken("{column3}", (int)TOKEN_LMD_ST::column3);
    tokenContainer->DefineStringToken("{column4}", (int)TOKEN_LMD_ST::column4);
    tokenContainer->DefineStringToken("{column5}", (int)TOKEN_LMD_ST::column5);
    tokenContainer->DefineStringToken("{column6}", (int)TOKEN_LMD_ST::column6);
    tokenContainer->DefineIntToken("{nummenuitem}", (int)TOKEN_LMD_IT::nummenuitem);
    tokenContainer->DefineIntToken("{current}", (int)TOKEN_LMD_IT::current);
    tokenContainer->DefineIntToken("{separator}", (int)TOKEN_LMD_IT::separator);
    tokenContainer->DefineIntToken("{column2set}", (int)TOKEN_LMD_IT::column2set);
    tokenContainer->DefineIntToken("{column3set}", (int)TOKEN_LMD_IT::column3set);
    tokenContainer->DefineIntToken("{column4set}", (int)TOKEN_LMD_IT::column4set);
    tokenContainer->DefineIntToken("{column5set}", (int)TOKEN_LMD_IT::column5set);
    tokenContainer->DefineIntToken("{column6set}", (int)TOKEN_LMD_IT::column6set);
    tokenContainer->DefineIntToken("{column1x}", (int)TOKEN_LMD_IT::column1x);
    tokenContainer->DefineIntToken("{column2x}", (int)TOKEN_LMD_IT::column2x);
    tokenContainer->DefineIntToken("{column3x}", (int)TOKEN_LMD_IT::column3x);
    tokenContainer->DefineIntToken("{column4x}", (int)TOKEN_LMD_IT::column4x);
    tokenContainer->DefineIntToken("{column5x}", (int)TOKEN_LMD_IT::column5x);
    tokenContainer->DefineIntToken("{column6x}", (int)TOKEN_LMD_IT::column6x);
    tokenContainer->DefineIntToken("{column1width}", (int)TOKEN_LMD_IT::column1width);
    tokenContainer->DefineIntToken("{column2width}", (int)TOKEN_LMD_IT::column2width);
    tokenContainer->DefineIntToken("{column3width}", (int)TOKEN_LMD_IT::column3width);
    tokenContainer->DefineIntToken("{column4width}", (int)TOKEN_LMD_IT::column4width);
    tokenContainer->DefineIntToken("{column5width}", (int)TOKEN_LMD_IT::column5width);
    tokenContainer->DefineIntToken("{column6width}", (int)TOKEN_LMD_IT::column6width);
    tokenContainer->DefineIntToken("{column1pb}", (int)TOKEN_LMD_IT::column1pb);
    tokenContainer->DefineIntToken("{column2pb}", (int)TOKEN_LMD_IT::column2pb);
    tokenContainer->DefineIntToken("{column3pb}", (int)TOKEN_LMD_IT::column3pb);
    tokenContainer->DefineIntToken("{column4pb}", (int)TOKEN_LMD_IT::column4pb);
    tokenContainer->DefineIntToken("{column5pb}", (int)TOKEN_LMD_IT::column5pb);
    tokenContainer->DefineIntToken("{column6pb}", (int)TOKEN_LMD_IT::column6pb);
    tokenContainer->DefineIntToken("{column1pbtotalsize}", (int)TOKEN_LMD_IT::column1pbtotalsize);
    tokenContainer->DefineIntToken("{column2pbtotalsize}", (int)TOKEN_LMD_IT::column2pbtotalsize);
    tokenContainer->DefineIntToken("{column3pbtotalsize}", (int)TOKEN_LMD_IT::column3pbtotalsize);
    tokenContainer->DefineIntToken("{column4pbtotalsize}", (int)TOKEN_LMD_IT::column4pbtotalsize);
    tokenContainer->DefineIntToken("{column5pbtotalsize}", (int)TOKEN_LMD_IT::column5pbtotalsize);
    tokenContainer->DefineIntToken("{column6pbtotalsize}", (int)TOKEN_LMD_IT::column6pbtotalsize);
    tokenContainer->DefineIntToken("{column1pbsize}", (int)TOKEN_LMD_IT::column1pbsize);
    tokenContainer->DefineIntToken("{column2pbsize}", (int)TOKEN_LMD_IT::column2pbsize);
    tokenContainer->DefineIntToken("{column3pbsize}", (int)TOKEN_LMD_IT::column3pbsize);
    tokenContainer->DefineIntToken("{column4pbsize}", (int)TOKEN_LMD_IT::column4pbsize);
    tokenContainer->DefineIntToken("{column5pbsize}", (int)TOKEN_LMD_IT::column5pbsize);
    tokenContainer->DefineIntToken("{column6pbsize}", (int)TOKEN_LMD_IT::column6pbsize);
    tokenContainer->DefineIntToken("{unknown}", (int)TOKEN_LMD_IT::unknown);
    tokenContainer->DefineIntToken("{channeledit}", (int)TOKEN_LMD_IT::channeledit);
    tokenContainer->DefineIntToken("{timeredit}", (int)TOKEN_LMD_IT::timeredit);
    tokenContainer->DefineIntToken("{recordinginfo}", (int)TOKEN_LMD_IT::recordinginfo);
    tokenContainer->DefineIntToken("{recordingedit}", (int)TOKEN_LMD_IT::recordingedit);
    tokenContainer->DefineIntToken("{setup}", (int)TOKEN_LMD_IT::setup);
    tokenContainer->DefineIntToken("{commands}", (int)TOKEN_LMD_IT::commands);
    tokenContainer->DefineIntToken("{folder}", (int)TOKEN_LMD_IT::folder);
    tokenContainer->DefineIntToken("{cam}", (int)TOKEN_LMD_IT::cam);
    tokenContainer->DefineIntToken("{fritzbox}", (int)TOKEN_LMD_IT::fritzbox);
    tokenContainer->DefineIntToken("{systeminfo}", (int)TOKEN_LMD_IT::systeminfo);
    tokenContainer->DefineIntToken("{mailbox}", (int)TOKEN_LMD_IT::mailbox);
    tokenContainer->DefineIntToken("{neutrinoepg}", (int)TOKEN_LMD_IT::neutrinoepg);
    tokenContainer->DefineIntToken("{remotetimers}", (int)TOKEN_LMD_IT::remotetimers);
    tokenContainer->DefineIntToken("{zaphistory}", (int)TOKEN_LMD_IT::zaphistory);
    tokenContainer->DefineIntToken("{remoteosd}", (int)TOKEN_LMD_IT::remoteosd);
    tokenContainer->DefineIntToken("{filebrowser}", (int)TOKEN_LMD_IT::filebrowser);
    tokenContainer->DefineIntToken("{epgsearch}", (int)TOKEN_LMD_IT::epgsearch);
    tokenContainer->DefineIntToken("{devstatus}", (int)TOKEN_LMD_IT::devstatus);

    InheritTokenContainer();
}

void cLeMenuDefault::SetListInfo(int *colX, int *colWidths) {
    this->colX = colX;
    this->colWidths = colWidths;
}

void cLeMenuDefault::SetText(const char *text) {
    dirty = true;
    free(this->text);
    this->text = strdup(text);
}

bool cLeMenuDefault::Parse(bool forced) {
    if (!dirty)
        return false;

    tokenContainer->Clear();
    ADD_TOKEN_LMD_IT(nummenuitem, num);
    ADD_TOKEN_LMD_IT(current, current);
    ADD_TOKEN_LMD_IT(separator, !selectable);
    for (int i = 0; i < MAX_TABS; i++) {
        bool colUsed = false;
        const char *s = GetTabbedText(text, i);
        if (s) {
            tokenContainer->AddStringToken(i, s);
            colUsed = true;
            CheckProgressBar(s, i);
        }
        else
            tokenContainer->AddStringToken(i, "");
        if (i > 0)
            tokenContainer->AddIntToken(i + 2, colUsed);
        tokenContainer->AddIntToken(i + 8, colX[i]);
        tokenContainer->AddIntToken(i + 14, colWidths[i]);
    }
    SetMenuCategory();
    return true;
}

const char *cLeMenuDefault::GetTabbedText(const char *s, int tab) {
    if (!s)
        return NULL;
    static char buffer[1000];
    const char *a = s;
    const char *b = strchrnul(a, '\t');
    while (*b && tab-- > 0) {
        a = b + 1;
        b = strchrnul(a, '\t');
    }
    if (!*b)
        return (tab <= 0) ? a : NULL;
    unsigned int n = b - a;
    if (n >= sizeof(buffer))
        n = sizeof(buffer) - 1;
    strncpy(buffer, a, n);
    buffer[n] = 0;
    return buffer;
}

void cLeMenuDefault::SetMenuCategory(void) {
    switch (menuCat) {
        case mcUnknown:
        case mcUndefined:
            ADD_TOKEN_LMD_IT(unknown, 1);
            break;
        case mcChannelEdit:
            ADD_TOKEN_LMD_IT(channeledit, 1);
            break;
        case mcTimerEdit:
            ADD_TOKEN_LMD_IT(timeredit, 1);
            break;
        case mcRecordingInfo:
            ADD_TOKEN_LMD_IT(recordinginfo, 1);
            break;
        case mcRecordingEdit:
            ADD_TOKEN_LMD_IT(recordingedit, 1);
            break;
        case mcPluginSetup:
        case mcSetup:
        case mcSetupOsd:
        case mcSetupEpg:
        case mcSetupDvb:
        case mcSetupLnb:
        case mcSetupCam:
        case mcSetupRecord:
        case mcSetupReplay:
        case mcSetupMisc:
        case mcSetupPlugins:
            ADD_TOKEN_LMD_IT(setup, 1);
            return;
        case mcCommand:
            ADD_TOKEN_LMD_IT(commands, 1);
            return;
        case mcFolder:
            ADD_TOKEN_LMD_IT(folder, 1);
            break;
        case mcCam:
            ADD_TOKEN_LMD_IT(cam, 1);
            break;
        default:
            break;
    }

    if (!plugName) {
        return;
    }
    if (!strcmp(plugName, "fritzbox")) {
        ADD_TOKEN_LMD_IT(fritzbox, 1);
    } else if (!strcmp(plugName, "systeminfo")) {
        ADD_TOKEN_LMD_IT(systeminfo, 1);
    } else if (!strcmp(plugName, "mailbox")) {
        ADD_TOKEN_LMD_IT(mailbox, 1);
    } else if (!strcmp(plugName, "neutrinoepg")) {
        ADD_TOKEN_LMD_IT(neutrinoepg, 1);
    } else if (!strcmp(plugName, "remotetimers")) {
        ADD_TOKEN_LMD_IT(remotetimers, 1);
    } else if (!strcmp(plugName, "zaphistory")) {
        ADD_TOKEN_LMD_IT(zaphistory, 1);
    } else if (!strcmp(plugName, "remoteosd")) {
        ADD_TOKEN_LMD_IT(remoteosd, 1);
    } else if (!strcmp(plugName, "filebrowser")) {
        ADD_TOKEN_LMD_IT(filebrowser, 1);
    } else if (!strcmp(plugName, "epgsearch")) {
        ADD_TOKEN_LMD_IT(epgsearch, 1);
    } else if (!strcmp(plugName, "devstatus")) {
        ADD_TOKEN_LMD_IT(devstatus, 1);
    }
}

void cLeMenuDefault::CheckProgressBar(const char *text, int tab) {
    const char *start = strchr(text, '[');
    if (!start)
        return;
    const char *stop = strchr(text, ']');
    if (!stop)
        return;
    int total = stop - start - 1;
    int len = 0;
    char *s = (char*)start + 1;
    while (*s && *s == '|') {
        len++;
        s++;
    }
    tokenContainer->AddIntToken(20 + tab, 1);
    tokenContainer->AddIntToken(26 + tab, total);
    tokenContainer->AddIntToken(32 + tab, len);
}

void cLeMenuDefault::Clear(bool forceClearBackground) {
    if (current && currentElement) {
        currentElement->Close();
    }
    cViewElement::Close();
    dirty = false;
}
/******************************************************************
* cVeMenuMain
******************************************************************/
cVeMenuMain::cVeMenuMain(void) {
    text = NULL;
    number = NULL;
    label = NULL;
}

cVeMenuMain::~cVeMenuMain(void) {
    free(text);
    free(number);
    free(label);
}

int countDigits(int arg)
{
   return snprintf(NULL, 0, "%d", arg) - (arg < 0);
}

void cVeMenuMain::SplitText(void) {
   char* string = skipspace(text);
   unsigned int num = atoi(string);
   int digits = countDigits(num);

   free(number);
   free(label);

   if (digits > 0 && digits < 4 && string[digits] == ' ')
   {
      if (asprintf(&label, "%s", string+digits+1)) ;
      if (asprintf(&number, "%d", num)) ;
   }
   else
   {
      if (asprintf(&label, "%s", string)) ;
      number = strdup("");
   }
}
/*
void cVeMenuMain::SplitText(void) {
    char *start = skipspace(text);
    bool found = false;
    bool doBreak = false;
    size_t i = 0;
    char *c = start;
    while (*c) {
        if (i==0) {
            //if text directly starts with nonnumeric, break
            if (!(*c >= '0' && *c <= '9')) {
                break;
            }
        }
        if (found) {
            //if current char is not a figure anymore, break
            if (!(*c >= '0' && *c <= '9')) {
                //there has to be a space after the menu item number
                //plugins with figures in their name are eval :-)
                if (*c != ' ')
                    found = false;
                doBreak = true;
            }
        }
        if (*c >= '0' && *c <= '9') {
            found = true;
        }
        if (doBreak)
            break;
        if (i>4)
            break;
        c++;
        i++;
    }

    free(number);
    free(label);
    if (found) {
        number = (char*)malloc(i+1);
        memset(number, 0, i+1);
        strncpy(number, start, i);
    } else {
        number = (char*)malloc(2);
        memset(number, 0, 2);
        strncpy(number, "", 1);
    }
    label = strdup(skipspace(c));
}
*/

/******************************************************************
* cLeMenuMain
******************************************************************/
cLeMenuMain::cLeMenuMain(void) {
    currentMain = NULL;
}

cLeMenuMain::cLeMenuMain(const cLeMenuMain &other) : cListElement(other) {
    text = NULL;
    number = NULL;
    label = NULL;
    currentMain = NULL;
}

cLeMenuMain::~cLeMenuMain(void) {
}

void cLeMenuMain::SetTokenContainer(void) {
    tokenContainer = new skindesignerapi::cTokenContainer();

    // fill tokens common for eCeMenuMainIT, eCeMenuMainST, eLeMenuMainIT, eLeMenuMainST
    DEF_TOK_CONT_MM(TOKEN_LMM_ST);

    tokenContainer->DefineIntToken("{nummenuitem}", (int)TOKEN_LMM_IT::nummenuitem);
    tokenContainer->DefineIntToken("{current}", (int)TOKEN_LMM_IT::current);
    tokenContainer->DefineIntToken("{separator}", (int)TOKEN_LMM_IT::separator);

    InheritTokenContainer();
}

void cLeMenuMain::ClearCurrentElement(void) {
    if (wasCurrent && currentMain) {
        currentMain->Close();
    }
}

void cLeMenuMain::SetText(const char *text) {
    dirty = true;
    free(this->text);
    this->text = strdup(text);
}

bool cLeMenuMain::Parse(bool forced) {
    if (!dirty)
        return false;
    tokenContainer->Clear();
    ADD_TOKEN_LMM_IT(nummenuitem, num);
    ADD_TOKEN_LMM_IT(current, current);
    ADD_TOKEN_LMM_IT(separator, !selectable);
    SplitText();

    // do the same stuff as in cCeMenuMain::Parse()
    ADD_TOKEN_MM(TOKEN_LMM_ST);

    return true;
}

void cLeMenuMain::RenderCurrent(void) {
    if (!currentMain)
        return;
    currentMain->SetText(text);
    currentMain->SetListPosition(container.X(), container.Y(), container.Width(), container.Height(), num);
    currentMain->Parse();
}

const char *cLeMenuMain::PluginName(void) {
    if (!label)
        return NULL;
    for (int i = 0; ; i++) {
        cPlugin *p = cPluginManager::GetPlugin(i);
        if (p) {
            const char *mainMenuEntry = p->MainMenuEntry();
            if (!mainMenuEntry)
                continue;
            if (!strcmp(mainMenuEntry, label)) {
                return p->Name();
            }
        } else
            break;
    }
    return NULL;
}

/******************************************************************
* cCeMenuMain
******************************************************************/
cCeMenuMain::cCeMenuMain(void) {

}

cCeMenuMain::~cCeMenuMain(void) {

}

void cCeMenuMain::SetText(const char *text) {
    dirty = true;
    free(this->text);
    this->text = strdup(text);
}

void cCeMenuMain::SetTokenContainer(void) {
    tokenContainer = new skindesignerapi::cTokenContainer();

    tokenContainer->DefineIntToken("{menuitemx}", (int)TOKEN_CMM_IT::menuitemx);
    tokenContainer->DefineIntToken("{menuitemy}", (int)TOKEN_CMM_IT::menuitemy);
    tokenContainer->DefineIntToken("{menuitemwidth}", (int)TOKEN_CMM_IT::menuitemwidth);
    tokenContainer->DefineIntToken("{menuitemheight}", (int)TOKEN_CMM_IT::menuitemheight);
    tokenContainer->DefineIntToken("{numitem}", (int)TOKEN_CMM_IT::numitem);

    // fill tokens common for eCeMenuMainIT, eCeMenuMainST, eLeMenuMainIT, eLeMenuMainST
    DEF_TOK_CONT_MM(TOKEN_CMM_ST);
    InheritTokenContainer();
}

bool cCeMenuMain::Parse(bool forced) {
    if (!cViewElement::Parse(forced))
        return false;
    if (!dirty)
        return false;
    tokenContainer->Clear();
    SplitText();
    SetListTokens(tokenContainer);

    // do the same stuff as in cLeMenuMain::Parse()
    ADD_TOKEN_MM(TOKEN_CMM_ST);
    return true;
}

/******************************************************************
* cVeMenuSchedules
******************************************************************/
cVeMenuSchedules::cVeMenuSchedules(void) {
    event = NULL;
    channel = NULL;
    withDate = false;
    timerMatch = tmNone;
    epgSearchFav = false;
}

/******************************************************************
* cLeMenuSchedules
******************************************************************/
cLeMenuSchedules::cLeMenuSchedules(void) {
    currentSchedules = NULL;
    menuCat = mcUnknown;
}

cLeMenuSchedules::cLeMenuSchedules(const cLeMenuSchedules &other) : cListElement(other) {
    currentSchedules = NULL;
}

cLeMenuSchedules::~cLeMenuSchedules(void) {
}

void cLeMenuSchedules::SetTokenContainer(void) {
    tokenContainer = new skindesignerapi::cTokenContainer();

    // fill tokens common for eCeMenuSchedulesIT, eCeMenuSchedulesST, eLeMenuSchedulesIT, eLeMenuSchedulesST
    DEF_TOK_CONT_MS(TOKEN_LMS_IT, TOKEN_LMS_ST);

    tokenContainer->DefineStringToken("{durationminutes}", (int)TOKEN_LMS_ST::durationminutes);
    tokenContainer->DefineIntToken("{nummenuitem}", (int)TOKEN_LMS_IT::nummenuitem);
    tokenContainer->DefineIntToken("{current}", (int)TOKEN_LMS_IT::current);
    tokenContainer->DefineIntToken("{separator}", (int)TOKEN_LMS_IT::separator);
    tokenContainer->DefineIntToken("{istoday}", (int)TOKEN_LMS_IT::istoday);
    tokenContainer->DefineIntToken("{startsin}", (int)TOKEN_LMS_IT::startsin);
    tokenContainer->DefineIntToken("{hasVPS}", (int)TOKEN_LMS_IT::hasVPS);

    InheritTokenContainer();
}

void cLeMenuSchedules::ClearCurrentElement(void) {
    if (wasCurrent && currentSchedules) {
        currentSchedules->Close();
    }
}

void cLeMenuSchedules::Set(const cEvent *event, const cChannel *channel, bool withDate, eTimerMatch timerMatch) {
    dirty = true;
    this->event = event;
    this->channel = channel;
    this->withDate = withDate;
    this->timerMatch = timerMatch;
}

bool cLeMenuSchedules::Parse(bool forced) {
    if (!dirty)
        return false;

    tokenContainer->Clear();
    ADD_TOKEN_LMS_IT(nummenuitem, num);
    ADD_TOKEN_LMS_IT(current, current);
    ADD_TOKEN_LMS_IT(separator, !selectable);

    ADD_TOKEN_LMS_IT(whatson, (menuCat == mcSchedule && !epgSearchFav) ? true : false);
    ADD_TOKEN_LMS_IT(whatsonnow, menuCat == mcScheduleNow ? true : false);
    ADD_TOKEN_LMS_IT(whatsonnext, menuCat == mcScheduleNext ? true : false);
    ADD_TOKEN_LMS_IT(whatsonfavorites, epgSearchFav);

    if (timerMatch == tmFull) {
        ADD_TOKEN_LMS_IT(timerpartitial, false);
        ADD_TOKEN_LMS_IT(timerfull, true);
    } else if (timerMatch == tmPartial) {
        ADD_TOKEN_LMS_IT(timerpartitial, true);
        ADD_TOKEN_LMS_IT(timerfull, false);
    } else {
        ADD_TOKEN_LMS_IT(timerpartitial, false);
        ADD_TOKEN_LMS_IT(timerfull, false);
    }

    if (event) {
        if (selectable) {
            ADD_TOKEN_LMS_ST(title, event->Title());
            ADD_TOKEN_LMS_ST(shorttext, event->ShortText());
            ADD_TOKEN_LMS_ST(start, *(event->GetTimeString()));
            ADD_TOKEN_LMS_ST(stop, *(event->GetEndTimeString()));
            time_t startTime = event->StartTime();
            struct tm *sStartTime = localtime(&startTime);
            int day = sStartTime->tm_mday;
            int month = sStartTime->tm_mon;
            ADD_TOKEN_LMS_ST(day, *WeekDayName(startTime));
            ADD_TOKEN_LMS_ST(date, *ShortDateString(startTime));
            ADD_TOKEN_LMS_IT(year, sStartTime->tm_year + 1900);
            ADD_TOKEN_LMS_IT(daynumeric, day);
            ADD_TOKEN_LMS_IT(month, month + 1);

            bool isRunning = false;
            time_t now = time(NULL);
            if ((now >= event->StartTime()) && (now <= event->EndTime()))
                isRunning = true;
            ADD_TOKEN_LMS_IT(running, isRunning);
            if (isRunning) {
                ADD_TOKEN_LMS_IT(elapsed, (now - event->StartTime())/60);
                ADD_TOKEN_LMS_IT(startsin, 0);
            } else {
                ADD_TOKEN_LMS_IT(elapsed, 0);
                ADD_TOKEN_LMS_IT(startsin, (event->StartTime() - now)/60);
            }
            struct tm *sNow = localtime(&now);
            ADD_TOKEN_LMS_IT(istoday, (day == sNow->tm_mday && month == sNow->tm_mon) ? true : false);
            ADD_TOKEN_LMS_IT(duration, event->Duration() / 60);
            ADD_TOKEN_LMS_IT(durationhours, event->Duration() / 3600);
            ADD_TOKEN_LMS_ST(durationminutes, *cString::sprintf("%.2d", (event->Duration() / 60)%60));
            ADD_TOKEN_LMS_IT(hasVPS, (bool)event->Vps());

            if (timerMatch == tmFull || timerMatch == tmPartial) {
               cTimer_Detail_V1 data;
               data.eventid = event->EventID();
               data.hastimer = false;
               data.local = true;
               data.type = 'R';
               if (cPlugin* pEpg2Vdr = cPluginManager::GetPlugin("epg2vdr"))
                  pEpg2Vdr->Service(EPG2VDR_TIMER_DETAIL_SERVICE, &data);
               ADD_TOKEN_LMS_IT(isremotetimer, !data.local);
               char tp[2]; sprintf(tp, "%c", data.type);
               ADD_TOKEN_LMS_ST(timertype, tp);
            }
        } else {
            char *sep = ParseSeparator(event->Title());
            ADD_TOKEN_LMS_ST(title, sep);
            free(sep);
        }
    } else if (!channel) {
        //Hack für komischen patch von jinx
        //stringTokens.insert(pair<string,string>("title", dayseparator));
    }
    if (channel) {
        ADD_TOKEN_LMS_ST(channelname, channel->Name());
        cString channelID = channel->GetChannelID().ToString();
        ADD_TOKEN_LMS_ST(channelid, *channelID);
        ADD_TOKEN_LMS_IT(channelnumber, channel->Number());
        ADD_TOKEN_LMS_IT(channellogoexists, imgCache->LogoExists(*channelID));
        if (!event && !selectable) {
            ADD_TOKEN_LMS_ST(title, channel->Name());
        }
    }
    return true;
}

void cLeMenuSchedules::RenderCurrent(void) {
    if (!currentSchedules)
        return;
    currentSchedules->Set(event, channel, withDate, timerMatch, menuCat);
    currentSchedules->SetEpgSearchFav(epgSearchFav);
    currentSchedules->SetListPosition(container.X(), container.Y(), container.Width(), container.Height(), num);
    currentSchedules->Parse();
}

/******************************************************************
* cCeMenuSchedules
******************************************************************/
cCeMenuSchedules::cCeMenuSchedules(void) {
    schedulesIndex = -1;
}

cCeMenuSchedules::~cCeMenuSchedules(void) {
}

void cCeMenuSchedules::SetTokenContainer(void) {
    tokenContainer = new skindesignerapi::cTokenContainer();

    // fill tokens common for eCeMenuSchedulesIT, eCeMenuSchedulesST, eLeMenuSchedulesIT, eLeMenuSchedulesST
    DEF_TOK_CONT_MS(TOKEN_CMS_IT, TOKEN_CMS_ST);

    tokenContainer->DefineStringToken("{description}", (int)TOKEN_CMS_ST::description);

    tokenContainer->DefineIntToken("{menuitemx}", (int)TOKEN_CMS_IT::menuitemx);
    tokenContainer->DefineIntToken("{menuitemy}", (int)TOKEN_CMS_IT::menuitemy);
    tokenContainer->DefineIntToken("{menuitemwidth}", (int)TOKEN_CMS_IT::menuitemwidth);
    tokenContainer->DefineIntToken("{menuitemheight}", (int)TOKEN_CMS_IT::menuitemheight);
    tokenContainer->DefineIntToken("{numitem}", (int)TOKEN_CMS_IT::numitem);

    tokenContainer->DefineStringToken("{posterpath}", (int)TOKEN_CMS_ST::posterpath);
    tokenContainer->DefineStringToken("{bannerpath}", (int)TOKEN_CMS_ST::bannerpath);
    tokenContainer->DefineIntToken("{hasposter}", (int)TOKEN_CMS_IT::hasposter);
    tokenContainer->DefineIntToken("{posterwidth}", (int)TOKEN_CMS_IT::posterwidth);
    tokenContainer->DefineIntToken("{posterheight}", (int)TOKEN_CMS_IT::posterheight);
    tokenContainer->DefineIntToken("{hasbanner}", (int)TOKEN_CMS_IT::hasbanner);
    tokenContainer->DefineIntToken("{bannerwidth}", (int)TOKEN_CMS_IT::bannerwidth);
    tokenContainer->DefineIntToken("{bannerheight}", (int)TOKEN_CMS_IT::bannerheight);

    tokenContainer->DefineLoopToken("{schedule[title]}", (int)eCeMenuSchedulesLT::title);
    tokenContainer->DefineLoopToken("{schedule[shorttext]}", (int)eCeMenuSchedulesLT::shorttext);
    tokenContainer->DefineLoopToken("{schedule[start]}", (int)eCeMenuSchedulesLT::start);
    tokenContainer->DefineLoopToken("{schedule[stop]}", (int)eCeMenuSchedulesLT::stop);
    schedulesIndex = tokenContainer->LoopIndex("schedule");
    InheritTokenContainer();
}

void cCeMenuSchedules::Set(const cEvent *event, const cChannel *channel, bool withDate, eTimerMatch timerMatch, eMenuCategory menuCat) {
    dirty = true;
    this->menuCat = menuCat;
    this->event = event;
    this->channel = channel;
    this->withDate = withDate;
    this->timerMatch = timerMatch;
}

bool cCeMenuSchedules::Parse(bool forced) {
    if (!cViewElement::Parse(forced))
        return false;
    if (!dirty)
        return false;
    tokenContainer->Clear();
    SetListTokens(tokenContainer);
    ADD_TOKEN_CMS_IT(whatson, (menuCat == mcSchedule && !epgSearchFav) ? true : false);
    ADD_TOKEN_CMS_IT(whatsonnow, menuCat == mcScheduleNow ? true : false);
    ADD_TOKEN_CMS_IT(whatsonnext, menuCat == mcScheduleNext ? true : false);
    ADD_TOKEN_CMS_IT(whatsonfavorites, epgSearchFav);
    if (timerMatch == tmFull) {
        ADD_TOKEN_CMS_IT(timerpartitial, false);
        ADD_TOKEN_CMS_IT(timerfull, true);
    } else if (timerMatch == tmPartial) {
        ADD_TOKEN_CMS_IT(timerpartitial, true);
        ADD_TOKEN_CMS_IT(timerfull, false);
    } else {
        ADD_TOKEN_CMS_IT(timerpartitial, false);
        ADD_TOKEN_CMS_IT(timerfull, false);
    }

    if (event) {
        ADD_TOKEN_CMS_ST(title, event->Title());
        ADD_TOKEN_CMS_ST(shorttext, event->ShortText());
        ADD_TOKEN_CMS_ST(description, event->Description());
        ADD_TOKEN_CMS_ST(start, *(event->GetTimeString()));
        ADD_TOKEN_CMS_ST(stop, *(event->GetEndTimeString()));
        time_t startTime = event->StartTime();
        struct tm * sStartTime = localtime(&startTime);
        ADD_TOKEN_CMS_ST(day, *WeekDayName(startTime));
        ADD_TOKEN_CMS_ST(date, *ShortDateString(startTime));
        ADD_TOKEN_CMS_IT(year, sStartTime->tm_year + 1900);
        ADD_TOKEN_CMS_IT(daynumeric, sStartTime->tm_mday);
        ADD_TOKEN_CMS_IT(month, sStartTime->tm_mon+1);
        bool isRunning = false;
        time_t now = time(NULL);
        if ((now >= event->StartTime()) && (now <= event->EndTime()))
            isRunning = true;
        ADD_TOKEN_CMS_IT(running, isRunning);
        if (isRunning) {
            ADD_TOKEN_CMS_IT(elapsed, (now - event->StartTime())/60);
        } else {
            ADD_TOKEN_CMS_IT(elapsed, 0);
        }
        ADD_TOKEN_CMS_IT(duration, event->Duration() / 60);
        ADD_TOKEN_CMS_IT(durationhours, event->Duration() / 3600);
        ADD_TOKEN_CMS_IT(durationminutes, (event->Duration() / 60) % 60);

        if (timerMatch == tmFull || timerMatch == tmPartial) {
           cTimer_Detail_V1 data;
           data.eventid = event->EventID();
           data.hastimer = false;
           data.local = true;
           data.type = 'R';
           if (cPlugin* pEpg2Vdr = cPluginManager::GetPlugin("epg2vdr"))
              pEpg2Vdr->Service(EPG2VDR_TIMER_DETAIL_SERVICE, &data);
           ADD_TOKEN_CMS_IT(isremotetimer, !data.local);
           char tp[2]; sprintf(tp, "%c", data.type);
           ADD_TOKEN_CMS_ST(timertype, tp);
        }
        if (LoadFullScrapInfo(event, NULL))
           SetScraperPosterBanner(tokenContainer);
    }
    if (channel) {
        ADD_TOKEN_CMS_ST(channelname, channel->Name());
        cString channelID = channel->GetChannelID().ToString();
        ADD_TOKEN_CMS_ST(channelid, *channelID);
        ADD_TOKEN_CMS_IT(channelnumber, channel->Number());
        ADD_TOKEN_CMS_IT(channellogoexists, imgCache->LogoExists(*channelID));
    }

    if (menuCat == mcScheduleNow || menuCat == mcScheduleNext) {
        int eventsAvailable = 0;
#if defined (APIVERSNUM) && (APIVERSNUM >= 20301)
        LOCK_SCHEDULES_READ;
        const cSchedules* schedules = Schedules;
#else
        cSchedulesLock schedulesLock;
        const cSchedules* schedules = (cSchedules*)cSchedules::Schedules(schedulesLock);
#endif
        const cSchedule *schedule = schedules->GetSchedule(channel);
        if (schedule) {
            for (const cEvent *e = schedule->GetPresentEvent(); e; e = schedule->Events()->Next(e)) {
                eventsAvailable++;
                if (eventsAvailable == 10)
                    break;
            }
        }
        vector<int> loopInfo;
        loopInfo.push_back(eventsAvailable);
        tokenContainer->CreateLoopTokenContainer(&loopInfo);

        if (schedule) {
            int num = 0;
            for (const cEvent *e = schedule->GetPresentEvent(); e; e = schedule->Events()->Next(e)) {
                if (num < 2) {
                    num++;
                    continue;
                }
                tokenContainer->AddLoopToken(schedulesIndex, num-2, (int)eCeMenuChannelsLT::title, e->Title());
                tokenContainer->AddLoopToken(schedulesIndex, num-2, (int)eCeMenuChannelsLT::shorttext, e->ShortText());
                tokenContainer->AddLoopToken(schedulesIndex, num-2, (int)eCeMenuChannelsLT::start, *(e->GetTimeString()));
                tokenContainer->AddLoopToken(schedulesIndex, num-2, (int)eCeMenuChannelsLT::stop, *(e->GetEndTimeString()));
                num++;
                if (num > 11)
                    break;
            }
        }
    }
    return true;
}

/******************************************************************
* cLeMenuChannels
******************************************************************/
cLeMenuChannels::cLeMenuChannels(void) {
    currentChannel = NULL;
    channel = NULL;
    withProvider = false;
}

cLeMenuChannels::cLeMenuChannels(const cLeMenuChannels &other) : cListElement(other) {
    currentChannel = NULL;
    channel = NULL;
    withProvider = false;
}

cLeMenuChannels::~cLeMenuChannels(void) {
}

void cLeMenuChannels::SetTokenContainer(void) {
    tokenContainer = new skindesignerapi::cTokenContainer();

    // fill tokens common for eCeMenuChannelsIT, eCeMenuChannelsST, eLeMenuChannelsIT, eLeMenuChannelsST
    DEF_TOK_CONT_MC(TOKEN_LMC_IT, TOKEN_LMC_ST);

    tokenContainer->DefineIntToken("{nummenuitem}", (int)TOKEN_LMC_IT::nummenuitem);
    tokenContainer->DefineIntToken("{current}", (int)TOKEN_LMC_IT::current);
    tokenContainer->DefineIntToken("{separator}", (int)TOKEN_LMC_IT::separator);

    InheritTokenContainer();
}

void cLeMenuChannels::ClearCurrentElement(void) {
    if (wasCurrent && currentChannel) {
        currentChannel->Close();
    }
}

void cLeMenuChannels::Set(const cChannel *channel, bool withProvider) {
    dirty = true;
    this->channel = channel;
    this->withProvider = withProvider;
}

bool cLeMenuChannels::Parse(bool forced) {
    if (!dirty)
        return false;

    bool isRadio = !channel->Vpid() && channel->Apid(0);

    bool isHD = false;
    bool isUHD = false;
    switch (channel->Vtype()) {
	case 0x1b: // H.264
	    isHD = true;
	    break;
	case 0x24: // H.265
	    isHD = true;
	    isUHD = true;
	    break;
    };

    tokenContainer->Clear();
    ADD_TOKEN_LMC_IT(nummenuitem, num);
    ADD_TOKEN_LMC_IT(current, current);
    ADD_TOKEN_LMC_IT(separator, !selectable);

	// do the same stuff as in cCeMenuChannels::Parse()
	ADD_TOKEN_MC(TOKEN_LMC_IT, TOKEN_LMC_ST);

    //current schedule
#if defined (APIVERSNUM) && (APIVERSNUM >= 20301)
    LOCK_SCHEDULES_READ;
    const cSchedules* schedules = Schedules;
#else
    cSchedulesLock schedulesLock;
    const cSchedules* schedules = (cSchedules*)cSchedules::Schedules(schedulesLock);
#endif
    const cSchedule *schedule = schedules->GetSchedule(channel);
    if (schedule) {
        const cEvent *presentEvent = schedule->GetPresentEvent();
        if (presentEvent) {
            ADD_TOKEN_LMC_ST(presenteventtitle, presentEvent->Title());
            ADD_TOKEN_LMC_ST(presenteventstart, *presentEvent->GetTimeString());
            ADD_TOKEN_LMC_ST(presenteventstop, *presentEvent->GetEndTimeString());
        }
    }
    return true;
}

void cLeMenuChannels::RenderCurrent(void) {
    if (!currentChannel)
        return;
    currentChannel->Set(channel, withProvider);
    currentChannel->SetListPosition(container.X(), container.Y(), container.Width(), container.Height(), num);
    currentChannel->Parse();
}

/******************************************************************
* cCeMenuChannels
******************************************************************/
cCeMenuChannels::cCeMenuChannels(void) {
    schedulesIndex = -1;
    channel = NULL;
    withProvider = false;
}

cCeMenuChannels::~cCeMenuChannels(void) {
}

void cCeMenuChannels::SetTokenContainer(void) {
    tokenContainer = new skindesignerapi::cTokenContainer();

    // fill tokens common for eCeMenuChannelsIT, eCeMenuChannelsST, eLeMenuChannelsIT, eLeMenuChannelsST
    DEF_TOK_CONT_MC(TOKEN_CMC_IT, TOKEN_CMC_ST);

    tokenContainer->DefineStringToken("{presenteventshorttext}", (int)TOKEN_CMC_ST::presenteventshorttext);
    tokenContainer->DefineStringToken("{presenteventdescription}", (int)TOKEN_CMC_ST::presenteventdescription);
    tokenContainer->DefineStringToken("{presenteventday}", (int)TOKEN_CMC_ST::presenteventday);
    tokenContainer->DefineStringToken("{presenteventdate}", (int)TOKEN_CMC_ST::presenteventdate);
    tokenContainer->DefineStringToken("{presenteventdurationminutes}", (int)TOKEN_CMC_ST::presenteventdurationminutes);
    tokenContainer->DefineStringToken("{posterpath}", (int)TOKEN_CMC_ST::posterpath);
    tokenContainer->DefineStringToken("{bannerpath}", (int)TOKEN_CMC_ST::bannerpath);
    tokenContainer->DefineStringToken("{nexteventtitle}", (int)TOKEN_CMC_ST::nexteventtitle);
    tokenContainer->DefineStringToken("{nexteventstart}", (int)TOKEN_CMC_ST::nexteventstart);
    tokenContainer->DefineStringToken("{nexteventstop}", (int)TOKEN_CMC_ST::nexteventstop);
    tokenContainer->DefineStringToken("{nexteventshorttext}", (int)TOKEN_CMC_ST::nexteventshorttext);
    tokenContainer->DefineStringToken("{nexteventdescription}", (int)TOKEN_CMC_ST::nexteventdescription);
    tokenContainer->DefineStringToken("{nexteventdurationminutes}", (int)TOKEN_CMC_ST::nexteventdurationminutes);
    tokenContainer->DefineStringToken("{nexteventday}", (int)TOKEN_CMC_ST::nexteventday);
    tokenContainer->DefineStringToken("{nexteventdate}", (int)TOKEN_CMC_ST::nexteventdate);
    tokenContainer->DefineIntToken("{menuitemx}", (int)TOKEN_CMC_IT::menuitemx);
    tokenContainer->DefineIntToken("{menuitemy}", (int)TOKEN_CMC_IT::menuitemy);
    tokenContainer->DefineIntToken("{menuitemwidth}", (int)TOKEN_CMC_IT::menuitemwidth);
    tokenContainer->DefineIntToken("{menuitemheight}", (int)TOKEN_CMC_IT::menuitemheight);
    tokenContainer->DefineIntToken("{numitem}", (int)TOKEN_CMC_IT::numitem);

    tokenContainer->DefineIntToken("{presenteventelapsed}", (int)TOKEN_CMC_IT::presenteventelapsed);
    tokenContainer->DefineIntToken("{presenteventduration}", (int)TOKEN_CMC_IT::presenteventduration);
    tokenContainer->DefineIntToken("{presenteventdurationhours}", (int)TOKEN_CMC_IT::presenteventdurationhours);
    tokenContainer->DefineIntToken("{hasposter}", (int)TOKEN_CMC_IT::hasposter);
    tokenContainer->DefineIntToken("{posterwidth}", (int)TOKEN_CMC_IT::posterwidth);
    tokenContainer->DefineIntToken("{posterheight}", (int)TOKEN_CMC_IT::posterheight);
    tokenContainer->DefineIntToken("{hasbanner}", (int)TOKEN_CMC_IT::hasbanner);
    tokenContainer->DefineIntToken("{bannerwidth}", (int)TOKEN_CMC_IT::bannerwidth);
    tokenContainer->DefineIntToken("{bannerheight}", (int)TOKEN_CMC_IT::bannerheight);
    tokenContainer->DefineIntToken("{nexteventduration}", (int)TOKEN_CMC_IT::nexteventduration);
    tokenContainer->DefineIntToken("{nexteventdurationhours}", (int)TOKEN_CMC_IT::nexteventdurationhours);
    tokenContainer->DefineLoopToken("{schedule[title]}", (int)eCeMenuChannelsLT::title);
    tokenContainer->DefineLoopToken("{schedule[shorttext]}", (int)eCeMenuChannelsLT::shorttext);
    tokenContainer->DefineLoopToken("{schedule[start]}", (int)eCeMenuChannelsLT::start);
    tokenContainer->DefineLoopToken("{schedule[stop]}", (int)eCeMenuChannelsLT::stop);
    schedulesIndex = tokenContainer->LoopIndex("schedule");
    InheritTokenContainer();
}

void cCeMenuChannels::Set(const cChannel *channel, bool withProvider) {
    dirty = true;
    this->channel = channel;
    this->withProvider = withProvider;
}

bool cCeMenuChannels::Parse(bool forced) {
    if (!cViewElement::Parse(forced))
        return false;
    if (!dirty)
        return false;

    bool isRadio = !channel->Vpid() && channel->Apid(0);

    bool isHD = false;
    bool isUHD = false;
    switch (channel->Vtype()) {
	case 0x1b: // H.264
	    isHD = true;
	    break;
	case 0x24: // H.265
	    isHD = true;
	    isUHD = true;
	    break;
    };

    tokenContainer->Clear();

    SetListTokens(tokenContainer);

    // do the same stuff as in cLeMenuChannels::Parse()
    ADD_TOKEN_MC(TOKEN_CMC_IT, TOKEN_CMC_ST);

#if defined (APIVERSNUM) && (APIVERSNUM >= 20301)
    LOCK_SCHEDULES_READ;
    const cSchedules* schedules = Schedules;
#else
    cSchedulesLock schedulesLock;
    const cSchedules* schedules = (cSchedules*)cSchedules::Schedules(schedulesLock);
#endif
    const cSchedule *schedule = schedules->GetSchedule(channel);
    if (schedule) {
        const cEvent *presentEvent = schedule->GetPresentEvent();
        if (presentEvent) {
            ADD_TOKEN_CMC_ST(presenteventtitle, presentEvent->Title());
            ADD_TOKEN_CMC_ST(presenteventshorttext, presentEvent->ShortText());
            ADD_TOKEN_CMC_ST(presenteventdescription, presentEvent->Description());
            ADD_TOKEN_CMC_ST(presenteventstart, *presentEvent->GetTimeString());
            ADD_TOKEN_CMC_ST(presenteventstop, *presentEvent->GetEndTimeString());
            ADD_TOKEN_CMC_ST(presenteventday, *WeekDayName(presentEvent->StartTime()));
            ADD_TOKEN_CMC_ST(presenteventdate, *ShortDateString(presentEvent->StartTime()));
            ADD_TOKEN_CMC_IT(presenteventelapsed, (time(0) - presentEvent->StartTime())/60);
            ADD_TOKEN_CMC_IT(presenteventduration, presentEvent->Duration() / 60);
            ADD_TOKEN_CMC_IT(presenteventdurationhours, presentEvent->Duration() / 3600);
            ADD_TOKEN_CMC_ST(presenteventdurationminutes, *cString::sprintf("%.2d", (presentEvent->Duration() / 60)%60));
            if (LoadFullScrapInfo(presentEvent, NULL))
                SetScraperPosterBannerChannel(tokenContainer);
        }
        const cList<cEvent> *events = schedule->Events();
        if (events && presentEvent) {
            const cEvent *nextEvent = events->Next(presentEvent);
            if (nextEvent) {
                ADD_TOKEN_CMC_ST(nexteventtitle, nextEvent->Title());
                ADD_TOKEN_CMC_ST(nexteventshorttext, nextEvent->ShortText());
                ADD_TOKEN_CMC_ST(nexteventdescription, nextEvent->Description());
                ADD_TOKEN_CMC_ST(nexteventstart, *nextEvent->GetTimeString());
                ADD_TOKEN_CMC_ST(nexteventstop, *nextEvent->GetEndTimeString());
                ADD_TOKEN_CMC_ST(nexteventday, *WeekDayName(nextEvent->StartTime()));
                ADD_TOKEN_CMC_ST(nexteventdate, *ShortDateString(nextEvent->StartTime()));
                ADD_TOKEN_CMC_IT(nexteventduration, nextEvent->Duration() / 60);
                ADD_TOKEN_CMC_IT(nexteventdurationhours, nextEvent->Duration() / 3600);
                ADD_TOKEN_CMC_ST(nexteventdurationminutes, *cString::sprintf("%.2d", (nextEvent->Duration() / 60)%60));
            }
        }
    }

    vector<int> loopInfo;
    int eventsAvailable = 0;
    if (schedule) {
        for (const cEvent *e = schedule->GetPresentEvent(); e; e = schedule->Events()->Next(e)) {
            eventsAvailable++;
            if (eventsAvailable == 12)
                break;
        }
    }
    eventsAvailable -= 2;
    if (eventsAvailable < 0)
        eventsAvailable = 0;

    loopInfo.push_back(eventsAvailable);
    tokenContainer->CreateLoopTokenContainer(&loopInfo);

    if (schedule && eventsAvailable > 0) {
        int num = 0;
        for (const cEvent *e = schedule->GetPresentEvent(); e; e = schedule->Events()->Next(e)) {
            if (num < 2) {
                num++;
                continue;
            }
            tokenContainer->AddLoopToken(schedulesIndex, num-2, (int)eCeMenuChannelsLT::title, e->Title());
            tokenContainer->AddLoopToken(schedulesIndex, num-2, (int)eCeMenuChannelsLT::shorttext, e->ShortText());
            tokenContainer->AddLoopToken(schedulesIndex, num-2, (int)eCeMenuChannelsLT::start, *(e->GetTimeString()));
            tokenContainer->AddLoopToken(schedulesIndex, num-2, (int)eCeMenuChannelsLT::stop, *(e->GetEndTimeString()));
            num++;
            if (num > 11)
                break;
        }
    }
    return true;
}

/******************************************************************
* cLeMenuTimers
******************************************************************/
cLeMenuTimers::cLeMenuTimers(void) {
    currentTimer = NULL;
    timer = NULL;
}

cLeMenuTimers::cLeMenuTimers(const cLeMenuTimers &other) : cListElement(other) {
    currentTimer = NULL;
    timer = NULL;
}

cLeMenuTimers::~cLeMenuTimers(void) {
}

void cLeMenuTimers::SetTokenContainer(void) {
    tokenContainer = new skindesignerapi::cTokenContainer();

    // fill tokens common for eCeMenuTimersIT, eCeMenuTimersST, eLeMenuTimersIT, eLeMenuTimersST
    DEF_TOK_CONT_MT(TOKEN_LMT_IT, TOKEN_LMT_ST);

    tokenContainer->DefineIntToken("{current}", (int)TOKEN_LMT_IT::current);
    tokenContainer->DefineIntToken("{separator}", (int)TOKEN_LMT_IT::separator);
    InheritTokenContainer();
}

void cLeMenuTimers::ClearCurrentElement(void) {
   if (wasCurrent && currentTimer) {
        currentTimer->Close();
    }
}

void cLeMenuTimers::Set(const cTimer *timer) {
    this->timer = timer;
    dirty = true;
}

bool cLeMenuTimers::Parse(bool forced) {
    if (!dirty)
        return false;

    tokenContainer->Clear();
    ADD_TOKEN_LMT_IT(nummenuitem, num);
    ADD_TOKEN_LMT_IT(current, current);
    ADD_TOKEN_LMT_IT(separator, !selectable);

    // do the same stuff as in cCeMenuTimers::Parse()
    ADD_TOKEN_MT(TOKEN_LMT_IT);

    const char *file = Setup.FoldersInTimerMenu ? NULL : strrchr(timer->File(), FOLDERDELIMCHAR);
    if (file && strcmp(file + 1, TIMERMACRO_TITLE) && strcmp(file + 1, TIMERMACRO_EPISODE))
        file++;
    else
        file = timer->File();
    ADD_TOKEN_LMT_ST(title, file);
    ADD_TOKEN_LMT_ST(timerstart, *cString::sprintf("%02d:%02d", timer->Start() / 100, timer->Start() % 100));
    ADD_TOKEN_LMT_ST(timerstop, *cString::sprintf("%02d:%02d", timer->Stop() / 100, timer->Stop() % 100));

    cString day("");
    cString dayName("");
    if (timer->WeekDays())
        day = timer->PrintDay(0, timer->WeekDays(), false);
    else if (timer->Day() - time(NULL) < 28 * SECSINDAY) {
        day = itoa(timer->GetMDay(timer->Day()));
        dayName = WeekDayName(timer->Day());
    } else {
        struct tm tm_r;
        time_t Day = timer->Day();
        localtime_r(&Day, &tm_r);
        char buffer[16];
        strftime(buffer, sizeof(buffer), "%Y%m%d", &tm_r);
        day = buffer;
    }

    struct tm tm_r;
    time_t timerDate = timer->Day();
    localtime_r(&timerDate, &tm_r);
    char buffer[4];
    strftime(buffer, sizeof(buffer), "%m", &tm_r);
    int month = atoi(buffer);
    char buffer2[6];
    strftime(buffer2, sizeof(buffer2), "%b", &tm_r);
    char buffer3[6];
    strftime(buffer3, sizeof(buffer3), "%Y", &tm_r);
    int year = atoi(buffer3);

    ADD_TOKEN_LMT_ST(day, *day);
    ADD_TOKEN_LMT_ST(dayname, *dayName);
    ADD_TOKEN_LMT_IT(month, month);
    ADD_TOKEN_LMT_ST(monthname, buffer2);
    ADD_TOKEN_LMT_IT(year, year);

    const cChannel *channel = timer->Channel();
    if (channel) {
        ADD_TOKEN_LMT_ST(channelname, channel->Name());
        cString channelID = channel->GetChannelID().ToString();
        ADD_TOKEN_LMT_ST(channelid, *channelID);
        ADD_TOKEN_LMT_IT(channellogoexists, imgCache->LogoExists(*channelID));
        ADD_TOKEN_LMT_IT(channelnumber, channel->Number());
    }

    const cEvent *event = timer->Event();
    if (event) {
        ADD_TOKEN_LMT_ST(eventtitle, event->Title());
        ADD_TOKEN_LMT_ST(eventstart, *event->GetTimeString());
        ADD_TOKEN_LMT_ST(eventstop, *event->GetEndTimeString());
    }

    cEpgTimer_Interface_V1* epgTimer;
    if (epgTimer = dynamic_cast<cEpgTimer_Interface_V1*>((cTimer*)timer)) {
        ADD_TOKEN_LMT_IT(isvdrrunning, epgTimer->isVdrRunning());
        ADD_TOKEN_LMT_IT(isremote, epgTimer->isRemote());
        stringstream state;
        state << epgTimer->State();
        ADD_TOKEN_LMT_ST(state, state.str().c_str());
        ADD_TOKEN_LMT_ST(stateinfo, epgTimer->StateInfo());
        ADD_TOKEN_LMT_ST(vdrname, epgTimer->VdrName());
        char tp[2]; sprintf(tp, "%c", epgTimer->Type());
        ADD_TOKEN_LMT_ST(type, tp);
        stringstream action;
        action << epgTimer->Action();
        ADD_TOKEN_LMT_ST(action, action.str().c_str());
    }

    return true;
}

void cLeMenuTimers::RenderCurrent(void) {
    if (!currentTimer)
        return;
    currentTimer->Set(timer);
    currentTimer->SetListPosition(container.X(), container.Y(), container.Width(), container.Height(), num);
    currentTimer->Parse();
}

/******************************************************************
* cCeMenuTimers
******************************************************************/
cCeMenuTimers::cCeMenuTimers(void) {
    timer = NULL;
}

cCeMenuTimers::~cCeMenuTimers(void) {

}

void cCeMenuTimers::SetTokenContainer(void) {
    tokenContainer = new skindesignerapi::cTokenContainer();

    // fill tokens common for eCeMenuTimersIT, eCeMenuTimersST, eLeMenuTimersIT, eLeMenuTimersST
    DEF_TOK_CONT_MT(TOKEN_CMT_IT, TOKEN_CMT_ST);

    // fill tokens specific for eCeMenuTimersIT, eCeMenuTimersST
    tokenContainer->DefineStringToken("{eventshorttext}", (int)TOKEN_CMT_ST::eventshorttext);
    tokenContainer->DefineStringToken("{eventdescription}", (int)TOKEN_CMT_ST::eventdescription);
    tokenContainer->DefineStringToken("{posterpath}", (int)TOKEN_CMT_ST::posterpath);
    tokenContainer->DefineStringToken("{bannerpath}", (int)TOKEN_CMT_ST::bannerpath);
    tokenContainer->DefineIntToken("{menuitemx}", (int)TOKEN_CMT_IT::menuitemx);
    tokenContainer->DefineIntToken("{menuitemy}", (int)TOKEN_CMT_IT::menuitemy);
    tokenContainer->DefineIntToken("{menuitemwidth}", (int)TOKEN_CMT_IT::menuitemwidth);
    tokenContainer->DefineIntToken("{menuitemheight}", (int)TOKEN_CMT_IT::menuitemheight);
    tokenContainer->DefineIntToken("{numitem}", (int)TOKEN_CMT_IT::numitem);
    tokenContainer->DefineIntToken("{hasposter}", (int)TOKEN_CMT_IT::hasposter);
    tokenContainer->DefineIntToken("{posterwidth}", (int)TOKEN_CMT_IT::posterwidth);
    tokenContainer->DefineIntToken("{posterheight}", (int)TOKEN_CMT_IT::posterheight);
    tokenContainer->DefineIntToken("{hasbanner}", (int)TOKEN_CMT_IT::hasbanner);
    tokenContainer->DefineIntToken("{bannerwidth}", (int)TOKEN_CMT_IT::bannerwidth);
    tokenContainer->DefineIntToken("{bannerheight}", (int)TOKEN_CMT_IT::bannerheight);
    InheritTokenContainer();
}

void cCeMenuTimers::Set(const cTimer *timer) {
    dirty = true;
    this->timer = timer;
}

bool cCeMenuTimers::Parse(bool forced) {
    if (!cViewElement::Parse(forced))
        return false;
    if (!dirty)
        return false;

    tokenContainer->Clear();

    SetListTokens(tokenContainer);

    // do the same stuff as in cLeMenuTimers::Parse()
    ADD_TOKEN_MT(TOKEN_CMT_IT);

    const char *file = Setup.FoldersInTimerMenu ? NULL : strrchr(timer->File(), FOLDERDELIMCHAR);
    if (file && strcmp(file + 1, TIMERMACRO_TITLE) && strcmp(file + 1, TIMERMACRO_EPISODE))
        file++;
    else
        file = timer->File();
    ADD_TOKEN_CMT_ST(title, file);
    ADD_TOKEN_CMT_ST(timerstart, *cString::sprintf("%02d:%02d", timer->Start() / 100, timer->Start() % 100));
    ADD_TOKEN_CMT_ST(timerstop, *cString::sprintf("%02d:%02d", timer->Stop() / 100, timer->Stop() % 100));

    cString day("");
    cString dayName("");
    if (timer->WeekDays())
        day = timer->PrintDay(0, timer->WeekDays(), false);
    else if (timer->Day() - time(NULL) < 28 * SECSINDAY) {
        day = itoa(timer->GetMDay(timer->Day()));
        dayName = WeekDayName(timer->Day());
    } else {
        struct tm tm_r;
        time_t Day = timer->Day();
        localtime_r(&Day, &tm_r);
        char buffer[16];
        strftime(buffer, sizeof(buffer), "%Y%m%d", &tm_r);
        day = buffer;
    }

    struct tm tm_r;
    time_t timerDate = timer->Day();
    localtime_r(&timerDate, &tm_r);
    char buffer[4];
    strftime(buffer, sizeof(buffer), "%m", &tm_r);
    int month = atoi(buffer);
    char buffer2[6];
    strftime(buffer2, sizeof(buffer2), "%b", &tm_r);
    char buffer3[6];
    strftime(buffer3, sizeof(buffer3), "%Y", &tm_r);
    int year = atoi(buffer3);

    ADD_TOKEN_CMT_ST(day, *day);
    ADD_TOKEN_CMT_ST(dayname, *dayName);
    ADD_TOKEN_CMT_IT(month, month);
    ADD_TOKEN_CMT_ST(monthname, buffer2);
    ADD_TOKEN_CMT_IT(year, year);

    const cChannel *channel = timer->Channel();
    if (channel) {
        ADD_TOKEN_CMT_ST(channelname, channel->Name());
        cString channelID = channel->GetChannelID().ToString();
        ADD_TOKEN_CMT_ST(channelid, *channelID);
        ADD_TOKEN_CMT_IT(channellogoexists, imgCache->LogoExists(*channelID));
        ADD_TOKEN_CMT_IT(channelnumber, channel->Number());
    }

    const cEvent *event = timer->Event();
    if (event) {
        ADD_TOKEN_CMT_ST(eventtitle, event->Title());
        ADD_TOKEN_CMT_ST(eventstart, *event->GetTimeString());
        ADD_TOKEN_CMT_ST(eventstop, *event->GetEndTimeString());
        ADD_TOKEN_CMT_ST(eventshorttext, event->ShortText());
        ADD_TOKEN_CMT_ST(eventdescription, event->Description());
        if (LoadFullScrapInfo(event, NULL))
            SetScraperPosterBannerTimer(tokenContainer);
    }

    cEpgTimer_Interface_V1* epgTimer;
    if (epgTimer = dynamic_cast<cEpgTimer_Interface_V1*>((cTimer*)timer)) {
        ADD_TOKEN_CMT_IT(isvdrrunning, epgTimer->isVdrRunning());
        ADD_TOKEN_CMT_IT(isremote, epgTimer->isRemote());
        stringstream state;
        state << epgTimer->State();
        ADD_TOKEN_CMT_ST(state, state.str().c_str());
        ADD_TOKEN_CMT_ST(stateinfo, epgTimer->StateInfo());
        ADD_TOKEN_CMT_ST(vdrname, epgTimer->VdrName());
        char tp[2]; sprintf(tp, "%c", epgTimer->Type());
        ADD_TOKEN_CMT_ST(type, tp);
        stringstream action;
        action << epgTimer->Action();
        ADD_TOKEN_CMT_ST(action, action.str().c_str());
    }

    return true;
}

/******************************************************************
* cLeMenuRecordings
******************************************************************/
cLeMenuRecordings::cLeMenuRecordings(void) {
    currentRecording = NULL;
    recording = NULL;
    level = 0;
    total = 0;
    New = 0;
}

cLeMenuRecordings::cLeMenuRecordings(const cLeMenuRecordings &other) : cListElement(other) {
    currentRecording = NULL;
    recording = NULL;
    level = 0;
    total = 0;
    New = 0;
}

cLeMenuRecordings::~cLeMenuRecordings(void) {
    delete recording;
}

void cLeMenuRecordings::SetTokenContainer(void) {
    tokenContainer = new skindesignerapi::cTokenContainer();

    // fill tokens common for eCeMenuRecordingsIT, eCeMenuRecordingsST, eLeMenuRecordingsIT, eLeMenuRecordingsST
    DEF_TOK_CONT_MR(TOKEN_LMR_IT, TOKEN_LMR_ST);

    tokenContainer->DefineIntToken("{nummenuitem}", (int)TOKEN_LMR_IT::nummenuitem);
    tokenContainer->DefineIntToken("{current}", (int)TOKEN_LMR_IT::current);
    tokenContainer->DefineIntToken("{separator}", (int)TOKEN_LMR_IT::separator);

    InheritTokenContainer();
}

void cLeMenuRecordings::ClearCurrentElement(void) {
   if (wasCurrent && currentRecording) {
        currentRecording->Close();
    }
}

void cLeMenuRecordings::Set(const cRecording *recording, int level, int total, int New) {
    if (!this->recording) {
        this->recording = new cRecording(recording->FileName());
    }
    this->level = level;
    this->total = total;
    this->New = New;
    dirty = true;
}

bool cLeMenuRecordings::Parse(bool forced) {
    if (!dirty)
        return false;

    tokenContainer->Clear();
    ADD_TOKEN_LMR_IT(nummenuitem, num);
    ADD_TOKEN_LMR_IT(current, current);
    ADD_TOKEN_LMR_IT(separator, !selectable);
    bool isFolder = (total > 0) ? true : false;
    ADD_TOKEN_LMR_IT(folder, isFolder);
    char *recName = RecName(recording->Name(), level);
    ADD_TOKEN_LMR_ST(name, recName);

    const cRecording *usedRecording = recording;

    if (isFolder) {
        char *folderName = FolderName(recording->Name(), level);
        cRecordingsFolderInfo::cFolderInfo *folderInfo = recFolderInfo.Get(folderName);
        delete[] folderName;
        if (folderInfo) {
#if defined (APIVERSNUM) && (APIVERSNUM >= 20301)
            LOCK_RECORDINGS_READ;
            const cRecordings* recordings = Recordings;
#else
            cRecordings* recordings = &Recordings;
#endif
            const cRecording *newestRec = recordings->GetByName(*folderInfo->LatestFileName);
            if (newestRec) {
                usedRecording = newestRec;
            }
            delete folderInfo;
        }
    }

    ADD_TOKEN_LMR_IT(isnew, usedRecording->IsNew());

    int percSeen = 0;
#if APIVERSNUM < 20108
    percSeen = -1;
#else
    percSeen = 0;
    int framesSeen = usedRecording->GetResume();
    int framesTotal = usedRecording->NumFrames();
    if (framesTotal > 0) {
        percSeen = (double)framesSeen / (double)framesTotal * 100;
    }
#endif

    // do the same stuff as in cCeMenuRecordings::Parse() (part 1)
    int recDuration = usedRecording->LengthInSeconds();
    ADD_TOKEN_MR1(TOKEN_LMR_IT);

    const cEvent *event = NULL;
    const cRecordingInfo *info = usedRecording->Info();

    if (!info) {
        delete[] recName;
        return true;
    }
    event = info->GetEvent();
    if (!event) {
        delete[] recName;
        return true;
    }
    ADD_TOKEN_LMR_ST(epgname, info->Title() ? info->Title() : recName);
    delete[] recName;

    if (info) {
        if (info->ChannelName() && (strlen(info->ChannelName()) > 0)) {
            ADD_TOKEN_LMR_ST(recchannelname, info->ChannelName());
            ADD_TOKEN_LMR_ST(recchannelid, info->ChannelID().ToString());
        } else {
#if defined (APIVERSNUM) && (APIVERSNUM >= 20301)
            LOCK_CHANNELS_READ;
            const cChannel *channel = Channels->GetByChannelID(info->ChannelID());
#else
            const cChannel *channel = Channels.GetByChannelID(info->ChannelID());
#endif
            if (channel) {
                ADD_TOKEN_LMR_ST(recchannelname, channel->Name());
                ADD_TOKEN_LMR_ST(recchannelid, *channel->GetChannelID().ToString());
            }
        }
    }

    cString recDate = event->GetDateString();
    cString recTime = event->GetTimeString();
    if (strstr(*recDate, "1970")) {
        time_t start = usedRecording->Start();
        recDate = DateString(start);
        recTime = TimeString(start);
    }

    int errors = -1;
#if defined (APIVERSNUM) && (APIVERSNUM >= 20504)
    errors = info->Errors();
#endif

    // do the same stuff as in cCeMenuRecordings::Parse() (part 2)
    ADD_TOKEN_MR2(TOKEN_LMR_IT, TOKEN_LMR_ST);
    SetScraperRecordingPoster(tokenContainer, usedRecording, true);

    return true;
}

void cLeMenuRecordings::RenderCurrent(void) {
    if (!currentRecording)
        return;
    currentRecording->Set(recording, level, total, New);
    currentRecording->SetListPosition(container.X(), container.Y(), container.Width(), container.Height(), num);
    currentRecording->Parse();
}

void cLeMenuRecordings::Clear(bool forceClearBackground) {
    delete recording;
    recording = NULL;
    cListElement::Clear(forceClearBackground);
}

void cLeMenuRecordings::Close(void) {
    delete recording;
    recording = NULL;
    cListElement::Close();
}

char *cLeMenuRecordings::RecName(const char *path, int level) {
    char *recName;
    const char *startName = path;
    for (int i = 0; i < level; i++) {
        startName = strchr(startName, FOLDERDELIMCHAR);
        startName++;
    }
    if (*startName == '%')
        startName++;
    const char *endName = startName;
    if (endName = strchr(endName, FOLDERDELIMCHAR)) {
        int len = endName - startName;
        recName = new char[len+1];
        strncpy(recName, startName, len);
        recName[len] = 0;
    } else {
        recName = strdup(startName);
    }
    return recName;
}

char *cLeMenuRecordings::FolderName(const char *path, int level) {
    char *folderName;
    const char *endName = path;
    bool foundEnd = false;
    for (int i = 0; i <= level; i++) {
        endName = strchr(endName, FOLDERDELIMCHAR);
        if (!endName) {
            foundEnd = false;
        }
        foundEnd = true;
        endName++;
    }
    if (foundEnd) {
        int len = endName - path - 1;
        folderName = new char[len+1];
        strncpy(folderName, path, len);
        folderName[len] = 0;
    } else {
        folderName = strdup(path);
    }
    return folderName;
}

/******************************************************************
* cCeMenuRecordings
******************************************************************/
cCeMenuRecordings::cCeMenuRecordings(void) {
    recording = NULL;
    level = 0;
    total = 0;
    New = 0;
}

cCeMenuRecordings::~cCeMenuRecordings(void) {
}

void cCeMenuRecordings::SetTokenContainer(void) {
    tokenContainer = new skindesignerapi::cTokenContainer();

    // fill tokens common for eCeMenuRecordingsIT, eCeMenuRecordingsST, eLeMenuRecordingsIT, eLeMenuRecordingsST
    DEF_TOK_CONT_MR(TOKEN_CMR_IT, TOKEN_CMR_ST);

    tokenContainer->DefineIntToken("{menuitemx}", (int)TOKEN_CMR_IT::menuitemx);
    tokenContainer->DefineIntToken("{menuitemy}", (int)TOKEN_CMR_IT::menuitemy);
    tokenContainer->DefineIntToken("{menuitemwidth}", (int)TOKEN_CMR_IT::menuitemwidth);
    tokenContainer->DefineIntToken("{menuitemheight}", (int)TOKEN_CMR_IT::menuitemheight);
    tokenContainer->DefineIntToken("{numitem}", (int)TOKEN_CMR_IT::numitem);

    InheritTokenContainer();
}

void cCeMenuRecordings::Set(const cRecording *recording, int level, int total, int New) {
    this->recording = recording;
    this->level = level;
    this->total = total;
    this->New = New;
    dirty = true;
}

bool cCeMenuRecordings::Parse(bool forced) {
    if (!cViewElement::Parse(forced))
        return false;
    if (!dirty)
        return false;

    tokenContainer->Clear();
    SetListTokens(tokenContainer);
    bool isFolder = (total > 0) ? true : false;
    ADD_TOKEN_CMR_IT(folder, isFolder);

    string name = recording->Name() ? recording->Name() : "";
    string buffer = "";
    stringstream folderName;
    try {
        vector<string> tokens;
        istringstream f(name.c_str());
        string s;
        int i=0;
        while (getline(f, s, FOLDERDELIMCHAR)) {
            tokens.push_back(s);
            if (isFolder && i <= level) {
                if (i > 0)
                    folderName << FOLDERDELIMCHAR;
                folderName << s;
                i++;
            }
        }
        buffer = tokens.at(level);
        if (!isFolder && recording->IsEdited()) {
            buffer = buffer.substr(1);
        }
    } catch (...) {
        buffer = name.c_str();
    }

    const cRecording *usedRecording = recording;

    if (isFolder) {
        cRecordingsFolderInfo::cFolderInfo *folderInfo = recFolderInfo.Get(folderName.str().c_str());
        if (folderInfo) {
#if defined (APIVERSNUM) && (APIVERSNUM >= 20301)
            LOCK_RECORDINGS_READ;
            const cRecordings* recordings = Recordings;
#else
            cRecordings* recordings = &Recordings;
#endif
            const cRecording *newestRec = recordings->GetByName(*folderInfo->LatestFileName);
            if (newestRec) {
                usedRecording = newestRec;
            }
            delete folderInfo;
        }
    }

    ADD_TOKEN_CMR_ST(name, buffer.c_str());
    ADD_TOKEN_CMR_IT(isnew, usedRecording->IsNew());

    int percSeen = 0;
#if APIVERSNUM < 20108
    percSeen = -1;
#else
    percSeen = 0;
    int framesSeen = usedRecording->GetResume();
    int framesTotal = usedRecording->NumFrames();
    if (framesTotal > 0) {
        percSeen = (double)framesSeen / (double)framesTotal * 100;
    }
#endif

    // do the same stuff as in cLeMenuRecordings::Parse() (part 1)
    int recDuration = usedRecording->LengthInSeconds();
    ADD_TOKEN_MR1(TOKEN_CMR_IT);

    const cEvent *event = NULL;
    const cRecordingInfo *info = usedRecording->Info();

    if (!info) return true;
    event = info->GetEvent();
    if (!event) return true;

    if (info) {
        if (info->ChannelName() && (strlen(info->ChannelName()) > 0)) {
            ADD_TOKEN_CMR_ST(recchannelname, info->ChannelName());
            ADD_TOKEN_CMR_ST(recchannelid, info->ChannelID().ToString());
        } else {
#if defined (APIVERSNUM) && (APIVERSNUM >= 20301)
            LOCK_CHANNELS_READ;
            const cChannel *channel = Channels->GetByChannelID(info->ChannelID());
#else
            const cChannel *channel = Channels.GetByChannelID(info->ChannelID());
#endif
            if (channel) {
                ADD_TOKEN_CMR_ST(recchannelname, channel->Name());
                ADD_TOKEN_CMR_ST(recchannelid, *channel->GetChannelID().ToString());
	        }
	    }
    }

    ADD_TOKEN_CMR_ST(epgname, info->Title() ? info->Title() : buffer.c_str());
    cString recDate = event->GetDateString();
    cString recTime = event->GetTimeString();
    if (strstr(*recDate, "1970")) {
        time_t start = usedRecording->Start();
        recDate = DateString(start);
        recTime = TimeString(start);
    }

    int errors = -1;
#if defined (APIVERSNUM) && (APIVERSNUM >= 20504)
    errors = info->Errors();
#endif

    // do the same stuff as in cLeMenuRecordings::Parse() (part 2)
    ADD_TOKEN_MR2(TOKEN_CMR_IT, TOKEN_CMR_ST);
    SetScraperRecordingPoster(tokenContainer, usedRecording, false);

    return true;
}

/******************************************************************
* cLeMenuPlugin
******************************************************************/
cLeMenuPlugin::cLeMenuPlugin(void) {
    currentPlugin = NULL;
    plugId = -1;
    plugMenuId = -1;
}

cLeMenuPlugin::cLeMenuPlugin(const cLeMenuPlugin &other) : cListElement(other) {
    currentPlugin = NULL;
    plugId = -1;
    plugMenuId = -1;
}

cLeMenuPlugin::~cLeMenuPlugin(void) {
}

void cLeMenuPlugin::SetTokenContainer(void) {
    if (plugId == -1 || plugMenuId == -1)
        return;
    skindesignerapi::cTokenContainer *tkPlugMenu = plgManager->GetTokenContainer(plugId, plugMenuId);
    tokenContainer = new skindesignerapi::cTokenContainer(*tkPlugMenu);
    InheritTokenContainer();
}

void cLeMenuPlugin::ClearCurrentElement(void) {
    if (wasCurrent && currentPlugin) {
        currentPlugin->Close();
    }
}

void cLeMenuPlugin::Set(skindesignerapi::cTokenContainer *tk) {
    tokenContainer->Clear();
    tokenContainer->SetTokens(tk);
    dirty = true;
}

bool cLeMenuPlugin::Parse(bool forced) {
    if (!dirty)
        return false;
    tokenContainer->AddIntToken(0, current);
    return true;
}

void cLeMenuPlugin::RenderCurrent(void) {
    if (!currentPlugin)
        return;
    currentPlugin->Set(tokenContainer);
    currentPlugin->SetListPosition(container.X(), container.Y(), container.Width(), container.Height(), num);
    currentPlugin->Parse();
}

/******************************************************************
* cCeMenuPlugin
******************************************************************/
cCeMenuPlugin::cCeMenuPlugin(void) {
    plugId = -1;
    plugMenuId = -1;
}

cCeMenuPlugin::~cCeMenuPlugin(void) {
}

void cCeMenuPlugin::SetTokenContainer(void) {
    if (plugId == -1 || plugMenuId == -1)
        return;
    skindesignerapi::cTokenContainer *tkPlugMenu = plgManager->GetTokenContainer(plugId, plugMenuId);
    tokenContainer = new skindesignerapi::cTokenContainer(*tkPlugMenu);
    InheritTokenContainer();
}

void cCeMenuPlugin::Set(skindesignerapi::cTokenContainer *tk) {
    tokenContainer->Clear();
    tokenContainer->SetTokens(tk);
    dirty = true;
}

bool cCeMenuPlugin::Parse(bool forced) {
    if (!cViewElement::Parse(forced))
        return false;
    if (!dirty)
        return false;
    return true;
}

/******************************************************************
* cLeAudioTracks
******************************************************************/
cLeAudioTracks::cLeAudioTracks(void) {
    text = NULL;
}

cLeAudioTracks::~cLeAudioTracks(void) {
    free(text);
}

void cLeAudioTracks::SetTokenContainer(void) {
    tokenContainer = new skindesignerapi::cTokenContainer();
    tokenContainer->DefineStringToken("{title}", (int)TOKEN_LDT_ST::title);
    tokenContainer->DefineIntToken("{nummenuitem}", (int)TOKEN_LDT_IT::nummenuitem);
    tokenContainer->DefineIntToken("{current}", (int)TOKEN_LDT_IT::current);
    InheritTokenContainer();
}

void cLeAudioTracks::Set(const char *text) {
    if (!text)
        return;
    free(this->text);
    this->text = strdup(text);
    dirty = true;
}

bool cLeAudioTracks::Parse(bool forced) {
    if (!dirty)
        return false;
    tokenContainer->Clear();
    ADD_TOKEN_LDT_IT(nummenuitem, num);
    ADD_TOKEN_LDT_IT(current, current);
    ADD_TOKEN_LDT_ST(title, text);
    return true;
}

/******************************************************************
* cLeChannelList
******************************************************************/
cLeChannelList::cLeChannelList(void) {
    channel = NULL;
}

cLeChannelList::cLeChannelList(const cLeChannelList &other) : cListElement(other) {
    channel = NULL;
}

cLeChannelList::~cLeChannelList(void) {
}

void cLeChannelList::SetTokenContainer(void) {
    tokenContainer = new skindesignerapi::cTokenContainer();
    tokenContainer->DefineStringToken("{name}", (int)TOKEN_LCL_ST::name);
    tokenContainer->DefineStringToken("{channelid}", (int)TOKEN_LCL_ST::channelid);
    tokenContainer->DefineStringToken("{presenteventtitle}", (int)TOKEN_LCL_ST::presenteventtitle);
    tokenContainer->DefineStringToken("{presenteventstart}", (int)TOKEN_LCL_ST::presenteventstart);
    tokenContainer->DefineStringToken("{presenteventstop}", (int)TOKEN_LCL_ST::presenteventstop);
    tokenContainer->DefineStringToken("{nexteventtitle}", (int)TOKEN_LCL_ST::nexteventtitle);
    tokenContainer->DefineStringToken("{nexteventstart}", (int)TOKEN_LCL_ST::nexteventstart);
    tokenContainer->DefineStringToken("{nexteventstop}", (int)TOKEN_LCL_ST::nexteventstop);
    tokenContainer->DefineIntToken("{nummenuitem}", (int)TOKEN_LCL_IT::nummenuitem);
    tokenContainer->DefineIntToken("{current}", (int)TOKEN_LCL_IT::current);
    tokenContainer->DefineIntToken("{number}", (int)TOKEN_LCL_IT::number);
    tokenContainer->DefineIntToken("{channellogoexists}", (int)TOKEN_LCL_IT::channellogoexists);
    tokenContainer->DefineIntToken("{presenteventelapsed}", (int)TOKEN_LCL_IT::presenteventelapsed);
    tokenContainer->DefineIntToken("{presenteventremaining}", (int)TOKEN_LCL_IT::presenteventremaining);
    InheritTokenContainer();
}

void cLeChannelList::Set(const cChannel *channel) {
    dirty = true;
    this->channel = channel;
}

bool cLeChannelList::Parse(bool forced) {
    if (!cViewElement::Parse(forced))
        return false;
    if (!dirty)
        return false;
    tokenContainer->Clear();
    ADD_TOKEN_LCL_IT(nummenuitem, num);
    ADD_TOKEN_LCL_IT(current, current);
    ADD_TOKEN_LCL_IT(number, channel->Number());
    ADD_TOKEN_LCL_ST(name, channel->Name());
    cString channelID = channel->GetChannelID().ToString();
    ADD_TOKEN_LCL_ST(channelid, *channelID);
    ADD_TOKEN_LCL_IT(channellogoexists, imgCache->LogoExists(*channelID));

#if defined (APIVERSNUM) && (APIVERSNUM >= 20301)
    LOCK_SCHEDULES_READ;
    const cSchedules* schedules = Schedules;
#else
    cSchedulesLock schedulesLock;
    const cSchedules* schedules = (cSchedules*)cSchedules::Schedules(schedulesLock);
#endif
    const cSchedule *schedule = schedules->GetSchedule(channel);
    if (schedule) {
        const cEvent *presentEvent = schedule->GetPresentEvent();
        if (presentEvent) {
            ADD_TOKEN_LCL_ST(presenteventtitle, presentEvent->Title());
            ADD_TOKEN_LCL_ST(presenteventstart, *presentEvent->GetTimeString());
            ADD_TOKEN_LCL_ST(presenteventstop, *presentEvent->GetEndTimeString());
            ADD_TOKEN_LCL_IT(presenteventelapsed, (time(0) - presentEvent->StartTime())/60);
            ADD_TOKEN_LCL_IT(presenteventremaining, presentEvent->Duration()/60 - (time(0) - presentEvent->StartTime())/60);
        }
        const cList<cEvent> *events = schedule->Events();
        if (events && presentEvent) {
            const cEvent *nextEvent = events->Next(presentEvent);
            if (nextEvent) {
                ADD_TOKEN_LCL_ST(nexteventtitle, nextEvent->Title());
                ADD_TOKEN_LCL_ST(nexteventstart, *nextEvent->GetTimeString());
                ADD_TOKEN_LCL_ST(nexteventstop, *nextEvent->GetEndTimeString());
            }
        }
    }

    return true;
}

/******************************************************************
* cLeGroupList
******************************************************************/
cLeGroupList::cLeGroupList(void) {
    group = NULL;
    numChannels = 0;
}

cLeGroupList::cLeGroupList(const cLeGroupList &other) : cListElement(other) {
    group = NULL;
    numChannels = 0;
}

cLeGroupList::~cLeGroupList(void) {
}

void cLeGroupList::SetTokenContainer(void) {
    tokenContainer = new skindesignerapi::cTokenContainer();
    tokenContainer->DefineStringToken("{groupname}", (int)TOKEN_LGL_ST::groupname);
    tokenContainer->DefineIntToken("{numchannels}", (int)TOKEN_LGL_IT::numchannels);
    tokenContainer->DefineIntToken("{nummenuitem}", (int)TOKEN_LGL_IT::nummenuitem);
    tokenContainer->DefineIntToken("{current}", (int)TOKEN_LGL_IT::current);
    InheritTokenContainer();
}

void cLeGroupList::Set(const char *group, int numChannels) {
    dirty = true;
    this->group = group;
    this->numChannels = numChannels;
}

bool cLeGroupList::Parse(bool forced) {
    if (!cViewElement::Parse(forced))
        return false;
    if (!dirty)
        return false;
    tokenContainer->Clear();
    ADD_TOKEN_LGL_IT(nummenuitem, num);
    ADD_TOKEN_LGL_IT(current, current);
    ADD_TOKEN_LGL_IT(numchannels, numChannels);
    ADD_TOKEN_LGL_ST(groupname, group);
    return true;
}

// vim: ts=4 sw=4 et
